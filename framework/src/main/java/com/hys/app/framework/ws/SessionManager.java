package com.hys.app.framework.ws;

import com.hys.app.framework.context.app.AppTypeEnum;
import org.springframework.web.socket.WebSocketSession;

/**
 * websocket session管理
 * @author kingapex
 * Datetime: 2022-07-18 14:54
 */
public interface SessionManager {

    /**
     * 连接成功时增加session
     * @param session
     */
    boolean addSession(WebSocketSession session);

    /**
     * 关闭连接时移除session
     * @param session
     */
    boolean removeSession(WebSocketSession session);

    /**
     *
     * @param appType
     * @param userId
     * @return
     */
    WebSocketSession getSession(AppTypeEnum appType, Long userId);

}
