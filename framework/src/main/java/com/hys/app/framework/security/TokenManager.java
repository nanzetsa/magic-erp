package com.hys.app.framework.security;

import com.hys.app.framework.auth.AuthUser;
import com.hys.app.framework.auth.Token;
import com.hys.app.framework.auth.TokenParseException;

/**
 * token业务管理接口
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019/12/25
 */
public interface TokenManager {



    /**
     * 创建token
     * @param user
     * @return
     */
    Token create(AuthUser user);


    /**
     * 带有效期的接口
     * @param user
     * @param accessTokenExp 访问token的有效期，秒数
     * @param refreshTokenExp 刷新token的有效期，秒数
     * @return
     */
    Token create(AuthUser user, int accessTokenExp, int refreshTokenExp);




    /**
     * 解析token
     * @param token
     * @return 用户对象
     */
    <T>  T parse(Class<T> clz, String token) throws TokenParseException;


    /**
     * 创建token
     * @param user
     * @param tokenOutTime token超时时间
     * @param refreshTokenOutTime  refreshToken超时时间
     * @return
     */
    Token create(AuthUser user,Integer tokenOutTime,Integer refreshTokenOutTime);

}
