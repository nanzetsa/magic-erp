package com.hys.app.service.base.plugin.sms;

import java.util.List;
import java.util.Map;

import com.hys.app.model.base.vo.ConfigItem;
import com.hys.app.model.base.vo.SmsSendVO;
import com.hys.app.model.system.dos.SmsTemplate;

/**
 * 短信发送
 *
 * @author zh
 * @version v1.0
 * @since v1.0
 * 2018年3月23日 下午3:07:05
 */
public interface SmsPlatform {
    /**
     * 配置各个存储方案的参数
     *
     * @return 参数列表
     */
    List<ConfigItem> definitionConfigItem();

    /**
     * 发送短信事件
     *
     * @param smsSendVO 短信内容
     * @param param   其它参数
     * @return
     */
    boolean onSend(SmsSendVO smsSendVO, Map param);


    /**
     * 获取插件ID
     *
     * @return 插件beanId
     */
    String getPluginId();

    /**
     * 获取插件名称
     *
     * @return 插件名称
     */
    String getPluginName();

    /**
     * 短信网关是否开启
     *
     * @return 0 不开启  1 开启
     */
    Integer getIsOpen();

    /**
     * 校验模板是否可以修改
     * @param oldModel 修改前的模板
     * @return
     */
    void templateUpdateCheck(SmsTemplate oldModel);

    /**
     * 修改模板
     * @param oldModel 修改前的模板
     * @param newModel 要修改的模板
     */
    void updateTemplate(SmsTemplate oldModel, SmsTemplate newModel);


    /**
     * 模板是否需要审核
     * @return true：需要
     */
    boolean isNeedAudit();


    /**
     * 模板提交审核
     * @param model
     */
    void templateSubmitAudit(SmsTemplate model);


    /**
     * 模板审核回调
     * @param json
     * @return
     */
    Map templateAuditCallback(String json);

    /**
     * 短信发送回调模板
     * @param json
     * @return
     */
    Map smsSendCallback(String json);


    /**
     * 记录短信发送记录信息
     * @param smsSendVO
     * @param config
     */
    void insertRecord(SmsSendVO smsSendVO, Map config);
}
