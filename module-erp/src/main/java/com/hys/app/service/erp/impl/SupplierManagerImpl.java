package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.SupplierConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.SupplierMapper;
import com.hys.app.model.erp.dos.SupplierDO;
import com.hys.app.model.erp.dto.SupplierDTO;
import com.hys.app.model.erp.dto.SupplierQueryParams;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.SupplierVO;
import com.hys.app.service.erp.NoGenerateManager;
import com.hys.app.service.erp.SupplierManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 供应商业务层实现
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
@Service
public class SupplierManagerImpl extends BaseServiceImpl<SupplierMapper, SupplierDO> implements SupplierManager {

    @Autowired
    private SupplierConverter converter;

    @Autowired
    private NoGenerateManager noGenerateManager;

    @Override
    public WebPage<SupplierVO> list(SupplierQueryParams queryParams) {
        WebPage<SupplierDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(SupplierDTO supplierDTO) {
        SupplierDO supplierDO = converter.convert(supplierDTO);
        supplierDO.setCustomSn(noGenerateManager.generate(NoBusinessTypeEnum.Supplier));
        save(supplierDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(SupplierDTO supplierDTO) {
        updateById(converter.convert(supplierDTO));
    }

    @Override
    public SupplierVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {
        removeById(id);
    }

    @Override
    public List<SupplierVO> normalStatusList() {
        return converter.convert(baseMapper.selectNormalStatusList());
    }

    @Override
    public void updateDisable(Long id, Boolean disable) {
        baseMapper.updateDisable(id, disable);
    }

}

