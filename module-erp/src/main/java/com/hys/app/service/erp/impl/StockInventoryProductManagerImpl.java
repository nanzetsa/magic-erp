package com.hys.app.service.erp.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hys.app.framework.util.BeanUtil;
import com.hys.app.mapper.erp.StockInventoryProductMapper;
import com.hys.app.model.erp.dos.StockInventoryProduct;
import com.hys.app.model.erp.dto.StockInventoryProductDTO;
import com.hys.app.service.erp.StockInventoryProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 库存盘点单产品业务接口实现
 *
 * @author dmy
 * 2023-12-05
 */
@Service
public class StockInventoryProductManagerImpl extends ServiceImpl<StockInventoryProductMapper, StockInventoryProduct> implements StockInventoryProductManager {

    @Autowired
    private StockInventoryProductMapper stockInventoryProductMapper;

    /**
     * 新增库存盘点单产品信息
     *
     * @param inventoryId 库存盘点单ID
     * @param productList 产品信息集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void saveProduct(Long inventoryId, List<StockInventoryProductDTO> productList) {
        //先删除旧数据
        List<Long> inventoryIds = new ArrayList<>();
        inventoryIds.add(inventoryId);
        this.deleteProduct(inventoryIds);

        //再循环将商品信息入库
        for (StockInventoryProductDTO productDTO : productList) {
            StockInventoryProduct product = new StockInventoryProduct();
            BeanUtil.copyProperties(productDTO, product);
            product.setInventoryId(inventoryId);
            this.save(product);
        }
    }

    /**
     * 删除库存盘点单产品信息
     *
     * @param inventoryIds 库存盘点单ID集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteProduct(List<Long> inventoryIds) {
        LambdaQueryWrapper<StockInventoryProduct> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(StockInventoryProduct::getInventoryId, inventoryIds);
        this.stockInventoryProductMapper.delete(wrapper);
    }

    /**
     * 根据库存盘点单ID获取库存盘点单产品信息集合
     *
     * @param inventoryId 库存盘点单ID
     * @return
     */
    @Override
    public List<StockInventoryProduct> list(Long inventoryId) {
        return this.lambdaQuery()
                .eq(StockInventoryProduct::getInventoryId, inventoryId)
                .list();
    }
}
