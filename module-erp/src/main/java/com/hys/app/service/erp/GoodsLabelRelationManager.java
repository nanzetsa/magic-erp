package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.GoodsLabelRelationDO;
import com.hys.app.model.erp.vo.GoodsLabelRelationVO;
import com.hys.app.model.erp.dto.GoodsLabelRelationDTO;
import com.hys.app.model.erp.dto.GoodsLabelRelationQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 商品标签-商品关联表业务层接口
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
public interface GoodsLabelRelationManager extends BaseService<GoodsLabelRelationDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<GoodsLabelRelationVO> list(GoodsLabelRelationQueryParams queryParams);

    /**
     * 关联商品
     * @param goodsLabelRelationDTO
     */
    void relationGoods(GoodsLabelRelationDTO goodsLabelRelationDTO);

    List<GoodsLabelRelationDO> listByGoodsIds(List<Long> goodsIds);

    /**
     * 根据标签id删除
     * @param labelIds
     */
    void deleteByLabelIds(List<Long> labelIds);
}

