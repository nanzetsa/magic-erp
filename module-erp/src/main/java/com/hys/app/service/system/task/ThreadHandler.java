package com.hys.app.service.system.task;

@FunctionalInterface
public interface ThreadHandler {
    void run(Thread thread);
}
