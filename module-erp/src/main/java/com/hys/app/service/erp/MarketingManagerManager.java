package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.MarketingManagerDO;
import com.hys.app.model.erp.dto.MarketingManagerDTO;
import com.hys.app.model.erp.dto.MarketingManagerQueryParams;
import com.hys.app.model.erp.vo.MarketingManagerVO;

import java.util.Collection;
import java.util.List;

/**
 * 营销经理业务层接口
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
public interface MarketingManagerManager extends BaseService<MarketingManagerDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<MarketingManagerVO> list(MarketingManagerQueryParams queryParams);

    /**
     * 添加
     *
     * @param marketingManagerDTO
     */
    void add(MarketingManagerDTO marketingManagerDTO);

    /**
     * 编辑
     *
     * @param marketingManagerDTO
     */
    void edit(MarketingManagerDTO marketingManagerDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    MarketingManagerVO getDetail(Long id);

    /**
     * 删除
     *
     * @param id
     */
    void delete(List<Long> ids);

    /**
     * 根据部门id集合查询
     *
     * @param deptIds
     * @return
     */
    List<MarketingManagerDO> listByDeptIds(Collection<Long> deptIds);
}

