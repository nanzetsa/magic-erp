package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.WarehouseOutItemConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.WarehouseOutItemMapper;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import com.hys.app.model.erp.dto.WarehouseOutItemQueryParams;
import com.hys.app.model.erp.vo.WarehouseOutItemVO;
import com.hys.app.service.erp.WarehouseOutItemManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 出库单商品明细业务层实现
 *
 * @author 张崧
 * @since 2023-12-07 17:06:32
 */
@Service
public class WarehouseOutItemManagerImpl extends BaseServiceImpl<WarehouseOutItemMapper, WarehouseOutItemDO> implements WarehouseOutItemManager {

    @Autowired
    private WarehouseOutItemConverter converter;

    @Override
    public WebPage<WarehouseOutItemVO> list(WarehouseOutItemQueryParams queryParams) {
        WebPage<WarehouseOutItemDO> webPage = baseMapper.selectPage(queryParams);
        return converter.convert(webPage);
    }

    @Override
    public List<WarehouseOutItemDO> listByWarehouseOutId(Long warehouseOutId) {
        return lambdaQuery().eq(WarehouseOutItemDO::getWarehouseOutId, warehouseOutId).list();
    }

    @Override
    public void deleteByWarehouseOutId(List<Long> warehouseOutIds) {
        lambdaUpdate().in(WarehouseOutItemDO::getWarehouseOutId, warehouseOutIds).remove();
    }

    @Override
    public void increaseReturnNum(Long warehouseOutItemId, Integer returnNum) {
        lambdaUpdate().setSql("return_num = return_num + " + returnNum).eq(WarehouseOutItemDO::getId, warehouseOutItemId).update();
    }

}

