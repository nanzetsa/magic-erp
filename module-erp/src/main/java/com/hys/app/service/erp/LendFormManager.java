package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.LendFormDTO;
import com.hys.app.model.erp.dto.LendFormQueryParam;
import com.hys.app.model.erp.dto.LendFormReturnDTO;
import com.hys.app.model.erp.vo.LendFormVO;

import java.util.List;

/**
 * 商品借出单业务接口
 * @author dmy
 * 2023-12-05
 */
public interface LendFormManager {

    /**
     * 查询商品借出单分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage list(LendFormQueryParam params);

    /**
     * 新增商品借出单
     *
     * @param lendFormDTO 商品借出单信息
     */
    void add(LendFormDTO lendFormDTO);

    /**
     * 编辑商品借出单
     *
     * @param id 主键ID
     * @param lendFormDTO 商品借出单信息
     */
    void edit(Long id, LendFormDTO lendFormDTO);

    /**
     * 删除商品借出单
     *
     * @param ids 商品借出单主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 获取商品借出单详情
     *
     * @param id 主键ID
     * @return
     */
    LendFormVO getDetail(Long id);

    /**
     * 确认商品借出单
     *
     * @param ids 商品借出单主键ID集合
     */
    void confirm(List<Long> ids);

    /**
     * 商品借出单产品归还
     *
     * @param id 主键ID
     * @param lendFormReturnDTO 商品借出单产品归还信息
     */
    void returnProduct(Long id, LendFormReturnDTO lendFormReturnDTO);
}
