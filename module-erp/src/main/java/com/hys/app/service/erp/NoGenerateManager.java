package com.hys.app.service.erp;

import com.hys.app.model.erp.enums.NoBusinessTypeEnum;

/**
 * 编号生成业务层接口
 *
 * @author 张崧
 * 2023-12-05
 */
public interface NoGenerateManager {

    /**
     * 生成业务编号
     * @param noBusinessTypeEnum 业务编号枚举
     * @return 业务编号
     */
    String generate(NoBusinessTypeEnum noBusinessTypeEnum);

    /**
     * 生成业务编号
     * @param noBusinessTypeEnum 业务编号枚举
     * @param deptId 部门id
     * @return 业务编号
     */
    String generate(NoBusinessTypeEnum noBusinessTypeEnum, Long deptId);
}

