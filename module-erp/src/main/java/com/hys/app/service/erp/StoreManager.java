package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.StoreDO;
import com.hys.app.model.erp.vo.StoreVO;
import com.hys.app.model.erp.dto.StoreDTO;
import com.hys.app.model.erp.dto.StoreQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 门店业务层接口
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
public interface StoreManager extends BaseService<StoreDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<StoreVO> list(StoreQueryParams queryParams);

    /**
     * 添加
     * @param storeDTO
     */
    void add(StoreDTO storeDTO);

    /**
     * 编辑
     * @param storeDTO
     */
    void edit(StoreDTO storeDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    StoreVO getDetail(Long id);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);
}

