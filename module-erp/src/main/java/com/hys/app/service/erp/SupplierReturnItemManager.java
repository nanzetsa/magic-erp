package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import com.hys.app.model.erp.vo.SupplierReturnItemVO;
import com.hys.app.model.erp.dto.SupplierReturnItemDTO;
import com.hys.app.model.erp.dto.SupplierReturnItemQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 供应商退货项业务层接口
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
public interface SupplierReturnItemManager extends BaseService<SupplierReturnItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<SupplierReturnItemVO> list(SupplierReturnItemQueryParams queryParams);

    /**
     * 添加
     * @param supplierReturnItemDTO
     */
    void add(SupplierReturnItemDTO supplierReturnItemDTO);

    /**
     * 编辑
     * @param supplierReturnItemDTO
     */
    void edit(SupplierReturnItemDTO supplierReturnItemDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    SupplierReturnItemVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 根据供应商退货单id删除
     * @param supplierReturnIds
     */
    void deleteBySupplierReturnId(List<Long> supplierReturnIds);

    /**
     * 根据供应商退货单id查询
     * @param supplierReturnId
     */
    List<SupplierReturnItemDO> listBySupplierReturnId(Long supplierReturnId);

}

