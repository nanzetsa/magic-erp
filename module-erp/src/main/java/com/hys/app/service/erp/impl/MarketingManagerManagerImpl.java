package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.MarketingManagerConverter;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.framework.database.WebPage;
import com.hys.app.mapper.erp.MarketingManagerMapper;
import com.hys.app.model.erp.dos.MarketingManagerDO;
import com.hys.app.model.erp.vo.MarketingManagerVO;
import com.hys.app.model.erp.dto.MarketingManagerDTO;
import com.hys.app.model.system.dos.DeptDO;
import com.hys.app.service.erp.MarketingManagerManager;
import com.hys.app.model.erp.dto.MarketingManagerQueryParams;
import com.hys.app.service.system.DeptManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.hys.app.framework.util.CollectionUtils.convertList;

/**
 * 营销经理业务层实现
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@Service
public class MarketingManagerManagerImpl extends BaseServiceImpl<MarketingManagerMapper, MarketingManagerDO> implements MarketingManagerManager {

    @Autowired
    private MarketingManagerConverter converter;

    @Autowired
    private DeptManager deptManager;

    @Override
    public WebPage<MarketingManagerVO> list(MarketingManagerQueryParams queryParams) {
        WebPage<MarketingManagerDO> webPage = baseMapper.selectPage(queryParams);

        List<Long> deptIds = convertList(webPage.getData(), MarketingManagerDO::getDeptId, marketingManagerDO -> marketingManagerDO.getDeptId() != null);
        Map<Long, String> deptNameMap = deptManager.listAndConvertMap(deptIds, DeptDO::getId, DeptDO::getName);

        return converter.combination(webPage, deptNameMap);
    }
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(MarketingManagerDTO marketingManagerDTO) {
        save(converter.convert(marketingManagerDTO));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void edit(MarketingManagerDTO marketingManagerDTO) {
        updateById(converter.convert(marketingManagerDTO));
    }

    @Override
    public MarketingManagerVO getDetail(Long id) {
        return converter.convert(getById(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    public List<MarketingManagerDO> listByDeptIds(Collection<Long> deptIds) {
        return lambdaQuery().in(MarketingManagerDO::getDeptId, deptIds).list();
    }

}

