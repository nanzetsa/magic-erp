package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.StockTransferDO;
import com.hys.app.model.erp.dto.StockTransferStatisticsParam;
import com.hys.app.model.erp.vo.StockTransferVO;
import com.hys.app.model.erp.dto.StockTransferDTO;
import com.hys.app.model.erp.dto.StockTransferQueryParams;
import com.hys.app.framework.database.WebPage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 库存调拨业务层接口
 *
 * @author 张崧
 * @since 2023-12-12 11:59:06
 */
public interface StockTransferManager extends BaseService<StockTransferDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<StockTransferVO> list(StockTransferQueryParams queryParams);

    /**
     * 添加
     * @param stockTransferDTO
     */
    void add(StockTransferDTO stockTransferDTO);

    /**
     * 编辑
     * @param stockTransferDTO
     */
    void edit(StockTransferDTO stockTransferDTO);

    /**
     * 查询详情
     * @param id
     * @return 
     */
    StockTransferVO getDetail(Long id);

    /**
     * 删除
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 提交调拨单
     * @param id
     */
    void submit(Long id);

    /**
     * 撤销提交调拨单
     * @param id
     */
    void withdraw(Long id);

    /**
     * 调拨确认
     *
     * @param ids
     * @param handleById
     */
    void confirm(List<Long> ids, Long handleById);

    /**
     * 调拨退回
     * @param ids
     * @param handleBy
     */
    void reject(List<Long> ids, Long handleBy);

    /**
     * 查询库存调拨统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(StockTransferStatisticsParam params);

    /**
     * 导出库存调拨统计列表
     *
     * @param response
     * @param params 查询参数
     */
    void export(HttpServletResponse response, StockTransferStatisticsParam params);

    /**
     * 根据仓库id查询数量
     * @param warehouseId
     * @return
     */
    long countByWarehouseId(Long warehouseId);
}

