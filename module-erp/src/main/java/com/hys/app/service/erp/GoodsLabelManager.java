package com.hys.app.service.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import com.hys.app.model.erp.vo.GoodsLabelVO;
import com.hys.app.model.erp.dto.GoodsLabelDTO;
import com.hys.app.model.erp.dto.GoodsLabelQueryParams;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 商品标签业务层接口
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
public interface GoodsLabelManager extends BaseService<GoodsLabelDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<GoodsLabelVO> list(GoodsLabelQueryParams queryParams);

    /**
     * 添加
     * @param goodsLabelDTO 新增数据
     */
    void add(GoodsLabelDTO goodsLabelDTO);

    /**
     * 编辑
     * @param goodsLabelDTO 更新数据
     */
    void edit(GoodsLabelDTO goodsLabelDTO);

    /**
     * 查询详情
     * @param id 主键
     * @return 详情数据
     */
    GoodsLabelVO getDetail(Long id);

    /**
     * 删除
     * @param ids 主键集合
     */
    void delete(List<Long> ids);
}

