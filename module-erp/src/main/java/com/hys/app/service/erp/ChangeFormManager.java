package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.ChangeFormDTO;
import com.hys.app.model.erp.dto.ChangeFormQueryParam;
import com.hys.app.model.erp.dto.ChangeFormStatisticsQueryParam;
import com.hys.app.model.erp.vo.ChangeFormVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 换货单业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ChangeFormManager {

    /**
     * 查询换货单分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage list(ChangeFormQueryParam params);

    /**
     * 新增换货单
     *
     * @param changeFormDTO 换货单信息
     */
    void add(ChangeFormDTO changeFormDTO);

    /**
     * 编辑换货单
     *
     * @param id 主键ID
     * @param changeFormDTO 换货单信息
     */
    void edit(Long id, ChangeFormDTO changeFormDTO);

    /**
     * 删除换货单
     *
     * @param ids 换货单主键ID集合
     */
    void delete(List<Long> ids);

    /**
     * 获取换货单详情
     *
     * @param id 主键ID
     * @return
     */
    ChangeFormVO getDetail(Long id);

    /**
     * 审核换货单
     *
     * @param ids 换货单ID集合
     * @param status 审核状态 PASS：审核通过，REJECT：审核驳回
     * @param rejectReason 驳回原因
     */
    void audit(List<Long> ids, String status, String rejectReason);

    /**
     * 换货单提交审核
     *
     * @param ids 换货单ID集合
     */
    void submit(List<Long> ids);

    /**
     * 换货单撤销提交审核
     *
     * @param ids 换货单ID集合
     */
    void cancel(List<Long> ids);

    /**
     * 查询商品换货单商品统计分页列表数据
     *
     * @param params 查询参数
     * @return
     */
    WebPage statistics(ChangeFormStatisticsQueryParam params);

    /**
     * 导出商品换货单商品列表
     *
     * @param response
     * @param params
     */
    void export(HttpServletResponse response, ChangeFormStatisticsQueryParam params);
}
