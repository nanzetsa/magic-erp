package com.hys.app.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hys.app.service.system.RegionsManager;
import com.hys.app.framework.cache.Cache;
import com.hys.app.framework.rabbitmq.MessageSender;
import com.hys.app.framework.rabbitmq.MqMessage;
import com.hys.app.mapper.goods.ShipTemplateChildMapper;
import com.hys.app.mapper.goods.ShipTemplateMapper;
import com.hys.app.model.base.CachePrefix;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.goods.vo.ShipTemplateMsg;
import com.hys.app.model.system.vo.RegionVO;
import com.hys.app.model.system.dos.ShipTemplateChild;
import com.hys.app.model.system.dos.ShipTemplateDO;
import com.hys.app.model.system.vo.ShipTemplateChildSellerVO;
import com.hys.app.model.system.vo.ShipTemplateSellerVO;
import com.hys.app.service.system.ShipTemplateManager;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运费模版业务类
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 21:44:49
 */
@Service
public class ShipTemplateManagerImpl implements ShipTemplateManager {

    @Autowired
    private ShipTemplateMapper shipTemplateMapper;

    @Autowired
    private ShipTemplateChildMapper shipTemplateChildMapper;

    @Autowired
    private Cache cache;

    @Autowired
    private RegionsManager regionsManager;

    @Autowired
    private MessageSender messageSender;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ShipTemplateDO save(ShipTemplateSellerVO template) {
        //设置商家ID为当前登录商家ID
//        template.setSellerId(UserContext.getSeller().getSellerId());
        //新建运费模版实体对象
        ShipTemplateDO t = new ShipTemplateDO();
        //复制属性
        BeanUtils.copyProperties(template, t);
        //运费模板信息入库
        shipTemplateMapper.insert(t);

        //保存运费模板子模板
        List<ShipTemplateChildSellerVO> items = template.getItems();
        List<ShipTemplateChildSellerVO> freeItems = template.getFreeItems();

        //设置指定运费子模板
        this.addTemplateChildren(items, t.getId(), false);
        //设置指定免运费子模板
        this.addTemplateChildren(freeItems, t.getId(), true);

        //删除缓存中的运费模板缓存信息
        cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix());
        //发送消息
        this.messageSender.send(new MqMessage(AmqpExchange.SHIP_TEMPLATE_CHANGE, AmqpExchange.SHIP_TEMPLATE_CHANGE + "_ROUTING", new ShipTemplateMsg(t.getId(), 1)));
        return t;
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ShipTemplateDO edit(ShipTemplateSellerVO template) {
        //设置商家ID为当前登录商家ID
//        template.setSellerId(UserContext.getSeller().getSellerId());
        //新建运费模版实体对象
        ShipTemplateDO t = new ShipTemplateDO();
        //复制属性
        BeanUtils.copyProperties(template, t);
        //获取运费模板ID
        Long id = template.getId();
        //修改运费模板信息
        shipTemplateMapper.updateById(t);

        //删除子模板
        QueryWrapper<ShipTemplateChild> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("template_id", id);
        shipTemplateChildMapper.delete(deleteWrapper);

        //保存运费模板子模板
        List<ShipTemplateChildSellerVO> items = template.getItems();
        List<ShipTemplateChildSellerVO> freeItems = template.getFreeItems();

        //设置指定运费子模板
        this.addTemplateChildren(items, id, false);

        //设置指定免运费子模板
        this.addTemplateChildren(freeItems, id, true);

        //移除缓存某个VO
        this.cache.remove(CachePrefix.SHIP_TEMPLATE_ONE.getPrefix() + id);
        //删除缓存中的运费模板缓存信息
        this.cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix());
        //发送消息
        this.messageSender.send(new MqMessage(AmqpExchange.SHIP_TEMPLATE_CHANGE, AmqpExchange.SHIP_TEMPLATE_CHANGE + "_ROUTING", new ShipTemplateMsg(id, 2)));
        return t;
    }

    /**
     * 添加运费模板子模板
     */
    private void addTemplateChildren(List<ShipTemplateChildSellerVO> items, Long templateId, boolean isFree) {

        for (ShipTemplateChildSellerVO child : items) {

            ShipTemplateChild shipTemplateChild = new ShipTemplateChild();
            BeanUtils.copyProperties(child, shipTemplateChild);
            shipTemplateChild.setTemplateId(templateId);
            //获取地区id
            String area = child.getArea();

            Gson gson = new Gson();
            Map<String, Map> map = new HashMap();
            map = gson.fromJson(area, map.getClass());
            StringBuffer areaIdBuffer = new StringBuffer(",");
            // 获取所有的地区
            Object obj = this.cache.get(CachePrefix.REGIONALL.getPrefix() + 4);
            List<RegionVO> allRegions = null;
            if (obj == null) {
                obj = regionsManager.getRegionByDepth(4);
            }
            allRegions = (List<RegionVO>) obj;
            Map<String, RegionVO> regionsMap = new HashMap();
            //循环地区放到Map中，便于取出
            for (RegionVO region : allRegions) {
                regionsMap.put(region.getId() + "", region);
            }

            for (String key : map.keySet()) {
                //拼接地区id
                areaIdBuffer.append(key + ",");
                Map dto = map.get(key);
                //需要取出改地区下面所有的子地区
                RegionVO provinceRegion = regionsMap.get(key);
                List<RegionVO> cityRegionList = provinceRegion.getChildren();

                Map<String, RegionVO> cityRegionMap = new HashMap<>();
                for (RegionVO city : cityRegionList) {
                    cityRegionMap.put(city.getId() + "", city);
                }
                //判断下面的地区是否被全选
                if ((boolean) dto.get("selected_all")) {

                    //市
                    for (RegionVO cityRegion : cityRegionList) {

                        areaIdBuffer.append(cityRegion.getId() + ",");
                        List<RegionVO> regionList = cityRegion.getChildren();
                        //区
                        for (RegionVO region : regionList) {

                            areaIdBuffer.append(region.getId() + ",");
                            List<RegionVO> townList = region.getChildren();
                            //城镇
                            if (townList != null) {
                                for (RegionVO townRegion : townList) {

                                    areaIdBuffer.append(townRegion.getId() + ",");
                                }
                            }
                        }
                    }
                } else {
                    //没有全选，则看选中城市
                    Map<String, Map> citiesMap = (Map<String, Map>) dto.get("children");
                    for (String cityKey : citiesMap.keySet()) {

                        areaIdBuffer.append(cityKey + ",");

                        Map cityMap = citiesMap.get(cityKey);

                        RegionVO cityRegion = cityRegionMap.get(cityKey);
                        List<RegionVO> regionList = cityRegion.getChildren();
                        //某个城市如果全部选中，需要取出城市下面的子地区
                        if ((boolean) cityMap.get("selected_all")) {
                            //区
                            for (RegionVO region : regionList) {

                                areaIdBuffer.append(region.getId() + ",");
                                List<RegionVO> townList = region.getChildren();
                                //城镇
                                if (townList != null) {
                                    for (RegionVO townRegion : townList) {

                                        areaIdBuffer.append(townRegion.getId() + ",");
                                    }
                                }
                            }

                        } else {
                            //选中了某个城市下面的几个区
                            Map<String, Map> regionMap = (Map<String, Map>) cityMap.get("children");
                            for (String regionKey : regionMap.keySet()) {

                                areaIdBuffer.append(regionKey + ",");
                                for (RegionVO region : regionList) {
                                    if (("" + region.getId()).equals(regionKey)) {
                                        List<RegionVO> townList = region.getChildren();
                                        //城镇
                                        if (townList != null) {
                                            for (RegionVO townRegion : townList) {

                                                areaIdBuffer.append(townRegion.getId() + ",");
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            shipTemplateChild.setAreaId(areaIdBuffer.toString());
            shipTemplateChild.setIsFree(isFree);
            shipTemplateChildMapper.insert(shipTemplateChild);
        }

    }

    @Override
    public List<ShipTemplateSellerVO> getStoreTemplate() {
        //先从缓存中获取运费模板数据
        List<ShipTemplateSellerVO> list = (List<ShipTemplateSellerVO>) cache.get(CachePrefix.SHIP_TEMPLATE.getPrefix());
        //如果缓存中的运费模板数据为空，再从数据库中获取数据
        if (list == null) {
            list = shipTemplateMapper.selectShipTemplateList();

            if (list != null) {
                for (ShipTemplateSellerVO vo : list) {
                    QueryWrapper<ShipTemplateChild> wrapper = new QueryWrapper<>();
                    wrapper.select("first_company", "first_price", "continued_company", "continued_price", "area").eq("template_id", vo.getId());
                    List<ShipTemplateChild> children = shipTemplateChildMapper.selectList(wrapper);
                    List<ShipTemplateChildSellerVO> items = new ArrayList<>();
                    if (children != null) {
                        for (ShipTemplateChild child : children) {
                            ShipTemplateChildSellerVO childvo = new ShipTemplateChildSellerVO(child, true);
                            items.add(childvo);
                        }
                    }
                    vo.setItems(items);
                }
            }
            cache.put(CachePrefix.SHIP_TEMPLATE.getPrefix(), list);
        }

        return list;
    }

    @Override
    @Transactional(value = "memberTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long templateId) {
        //删除运费模板
        shipTemplateMapper.deleteById(templateId);
        //删除运费模板关联地区
        QueryWrapper<ShipTemplateChild> wrapper = new QueryWrapper<>();
        wrapper.eq("template_id", templateId);
        shipTemplateChildMapper.delete(wrapper);

        //移除缓存某个VO
        this.cache.remove(CachePrefix.SHIP_TEMPLATE_ONE.getPrefix() + templateId);
        //移除缓存中的运费模板信息
        this.cache.remove(CachePrefix.SHIP_TEMPLATE.getPrefix());
    }

    @Override
    public ShipTemplateSellerVO getFromDB(Long templateId) {

        ShipTemplateDO template = this.getOneDB(templateId);
        ShipTemplateSellerVO tpl = new ShipTemplateSellerVO();
        BeanUtils.copyProperties(template, tpl);

        //查询运费模板的子模板
        QueryWrapper<ShipTemplateChild> wrapper = new QueryWrapper<>();
        wrapper.select("first_company", "first_price", "continued_company", "continued_price", "area").eq("template_id", templateId);
        List<ShipTemplateChild> children = shipTemplateChildMapper.selectList(wrapper);

        List<ShipTemplateChildSellerVO> items = new ArrayList<>();
        if (children != null) {
            for (ShipTemplateChild child : children) {
                ShipTemplateChildSellerVO childvo = new ShipTemplateChildSellerVO(child, false);
                items.add(childvo);
            }
        }

        tpl.setItems(items);

        return tpl;
    }

    /**
     * 数据库中查询运费模板
     *
     * @param templateId
     * @return
     */
    private ShipTemplateDO getOneDB(Long templateId) {
        return shipTemplateMapper.selectById(templateId);
    }

}
