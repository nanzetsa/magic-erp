package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dto.GoodsDTO;
import com.hys.app.model.erp.dto.GoodsQueryParams;
import com.hys.app.model.erp.vo.GoodsVO;

import java.util.List;

/**
 * 商品业务层接口
 *
 * @author 张崧
 * 2023-12-26 15:56:58
 */
public interface GoodsManager extends BaseService<GoodsDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<GoodsVO> list(GoodsQueryParams queryParams);

    /**
     * 添加
     *
     * @param goodsDTO
     */
    void add(GoodsDTO goodsDTO);

    /**
     * 编辑
     *
     * @param goodsDTO
     */
    void edit(GoodsDTO goodsDTO);

    /**
     * 查询详情
     *
     * @param id
     * @return
     */
    GoodsVO getDetail(Long id);

    /**
     * 删除
     *
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 根据商品分类查询数量
     *
     * @param categoryId
     * @return
     */
    long countByCategoryId(Long categoryId);
}

