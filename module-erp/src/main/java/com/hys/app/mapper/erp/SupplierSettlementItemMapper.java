package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.SupplierSettlementItemDO;
import com.hys.app.model.erp.dto.SupplierSettlementItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 供应商结算单明细的Mapper
 *
 * @author 张崧
 * @since 2023-12-15 14:09:20
 */
public interface SupplierSettlementItemMapper extends BaseMapperX<SupplierSettlementItemDO> {

    default WebPage<SupplierSettlementItemDO> selectPage(SupplierSettlementItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(SupplierSettlementItemDO::getSupplierSettlementId, params.getSupplierSettlementId())
                .eqIfPresent(SupplierSettlementItemDO::getWarehouseEntryId, params.getWarehouseEntryId())
                .eqIfPresent(SupplierSettlementItemDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(SupplierSettlementItemDO::getProductId, params.getProductId())
                .eqIfPresent(SupplierSettlementItemDO::getProductName, params.getProductName())
                .eqIfPresent(SupplierSettlementItemDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(SupplierSettlementItemDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(SupplierSettlementItemDO::getProductPrice, params.getProductPrice())
                .eqIfPresent(SupplierSettlementItemDO::getProductNum, params.getProductNum())
                .eqIfPresent(SupplierSettlementItemDO::getTotalPrice, params.getTotalPrice())
                .orderByDesc(SupplierSettlementItemDO::getId)
                .page(params);
    }

}

