package com.hys.app.mapper.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.dos.MarketingManagerDO;
import com.hys.app.model.erp.dto.MarketingManagerQueryParams;

/**
 * 营销经理的Mapper
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
public interface MarketingManagerMapper extends BaseMapperX<MarketingManagerDO> {

    default WebPage<MarketingManagerDO> selectPage(MarketingManagerQueryParams params) {
        return lambdaQuery()
                .andX(StringUtil.notEmpty(params.getKeyword()),
                        w -> w.like(MarketingManagerDO::getMobile, params.getKeyword()).or()
                                .like(MarketingManagerDO::getRealName, params.getKeyword()))
                .eqIfPresent(MarketingManagerDO::getDisableFlag, params.getDisableFlag())
                .eqIfPresent(MarketingManagerDO::getRealName, params.getRealName())
                .eqIfPresent(MarketingManagerDO::getMobile, params.getMobile())
                .eqIfPresent(MarketingManagerDO::getDeptId, params.getDeptId())
                .orderByDesc(MarketingManagerDO::getId)
                .page(params);
    }

}

