package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.erp.dos.ProcurementContract;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 采购合同Mapper
 * @author dmy
 * 2023-12-05
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ProcurementContractMapper extends BaseMapper<ProcurementContract> {
}
