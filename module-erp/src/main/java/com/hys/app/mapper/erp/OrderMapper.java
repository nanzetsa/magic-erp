package com.hys.app.mapper.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dto.OrderQueryParams;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单 Mapper
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Mapper
public interface OrderMapper extends BaseMapperX<OrderDO> {

    default WebPage<OrderDO> selectPage(OrderQueryParams params) {
        return lambdaQuery()
                .andX(StringUtil.notEmpty(params.getKeywords()),
                        w -> w.like(OrderDO::getSn, params.getKeywords()).or().like(OrderDO::getMemberName, params.getKeywords()))
                .likeIfPresent(OrderDO::getSn, params.getSn())
                .eqIfPresent(OrderDO::getStatus, params.getStatus())
                .eqIfPresent(OrderDO::getPaymentStatus, params.getPaymentStatus())
                .eqIfPresent(OrderDO::getMemberId, params.getMemberId())
                .likeIfPresent(OrderDO::getMemberName, params.getMemberName())
                .betweenIfPresent(OrderDO::getOrderTime, params.getOrderTime())
                .eqIfPresent(OrderDO::getWarehouseId, params.getWarehouseId())
                .likeIfPresent(OrderDO::getWarehouseName, params.getWarehouseName())
                .eqIfPresent(OrderDO::getMarketingId, params.getMarketingId())
                .likeIfPresent(OrderDO::getMarketingName, params.getMarketingName())
                .likeIfPresent(OrderDO::getRemark, params.getRemark())
                .eqIfPresent(OrderDO::getDeliveryType, params.getDeliveryType())
                .eqIfPresent(OrderDO::getStoreId, params.getStoreId())
                .likeIfPresent(OrderDO::getStoreName, params.getStoreName())
                .eqIfPresent(OrderDO::getWarehouseOutFlag, params.getWarehouseOutFlag())
                .eqIfPresent(OrderDO::getWarehouseOutId, params.getWarehouseOutId())
                .eqIfPresent(OrderDO::getShipFlag, params.getShipFlag())
                .betweenIfPresent(OrderDO::getShipTime, params.getShipTime())
                .eqIfPresent(OrderDO::getLogisticsCompanyId, params.getLogisticsCompanyId())
                .likeIfPresent(OrderDO::getLogisticsCompanyName, params.getLogisticsCompanyName())
                .likeIfPresent(OrderDO::getLogisticsTrackingNumber, params.getLogisticsTrackingNumber())
                .eqIfPresent(OrderDO::getType, params.getType())
                .orderByDesc(OrderDO::getId)
                .page(params);
    }

}

