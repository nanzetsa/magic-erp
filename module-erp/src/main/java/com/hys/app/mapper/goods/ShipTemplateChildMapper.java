package com.hys.app.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.system.dos.ShipTemplateChild;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 运费模版详细配置Mapper接口
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-28
 */
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface ShipTemplateChildMapper extends BaseMapper<ShipTemplateChild> {

}
