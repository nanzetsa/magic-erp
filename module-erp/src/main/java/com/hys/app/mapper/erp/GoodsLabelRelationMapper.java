package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.wrapper.MPJLambdaWrapperX;
import com.hys.app.model.erp.dos.GoodsDO;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import com.hys.app.model.erp.vo.GoodsLabelRelationVO;
import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.GoodsLabelRelationDO;
import com.hys.app.model.erp.dto.GoodsLabelRelationQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 商品标签-商品关联表 Mapper
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Mapper
public interface GoodsLabelRelationMapper extends BaseMapperX<GoodsLabelRelationDO> {

    default WebPage<GoodsLabelRelationVO> selectPage(GoodsLabelRelationQueryParams params) {
        return selectJoinPage(params, GoodsLabelRelationVO.class, new MPJLambdaWrapperX<GoodsLabelRelationDO>()
                .leftJoin(GoodsDO.class, GoodsDO::getId, GoodsLabelRelationDO::getGoodsId)
                .leftJoin(GoodsLabelDO.class, GoodsLabelDO::getId, GoodsLabelRelationDO::getLabelId)
                .selectAll(GoodsDO.class)
                .selectAs(GoodsLabelDO::getName, GoodsLabelRelationVO::getLabelName)
                .eqIfPresent(GoodsLabelRelationDO::getLabelId, params.getLabelId())
                .orderByDesc(GoodsLabelRelationDO::getId)
        );
    }

}

