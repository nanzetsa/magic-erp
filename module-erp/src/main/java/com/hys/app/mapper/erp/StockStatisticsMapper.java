package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.cache.MybatisRedisCache;
import com.hys.app.model.erp.dos.StockStatistics;
import com.hys.app.model.erp.dto.StockStatisticsQueryParam;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库存统计Mapper
 *
 * @author dmy
 * 2023-12-05
 */
@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface StockStatisticsMapper extends BaseMapper<StockStatistics> {

    /**
     * 查询商品库存统计信息
     * @param objectPage
     * @param params
     * @return
     */
    IPage<StockStatistics> selectStockStatisticsPage(Page<Object> objectPage, @Param("param") StockStatisticsQueryParam params);

    /**
     * 查询商品库存统计信息
     * @param params
     * @return
     */
    List<StockStatistics> selectStockStatisticsPage(@Param("param") StockStatisticsQueryParam params);

}
