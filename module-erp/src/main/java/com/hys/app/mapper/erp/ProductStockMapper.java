package com.hys.app.mapper.erp;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.ProductStockDO;
import com.hys.app.model.erp.dto.ProductStockQueryParams;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.vo.ProductStockWarningVO;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存的Mapper
 *
 * @author 张崧
 * @since 2023-12-07 10:08:44
 */
public interface ProductStockMapper extends BaseMapperX<ProductStockDO> {

    default WebPage<ProductStockDO> selectPage(ProductStockQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(ProductStockDO::getWarehouseId, params.getWarehouseId())
                .eqIfPresent(ProductStockDO::getProductId, params.getProductId())
                .eqIfPresent(ProductStockDO::getNum, params.getNum())
                .orderByDesc(ProductStockDO::getId)
                .page(params);
    }

    default ProductStockDO selectStock(Long warehouseId, Long productId){
        return lambdaQuery()
                .eq(ProductStockDO::getWarehouseId, warehouseId)
                .eq(ProductStockDO::getProductId, productId)
                .one();
    }

    /**
     * 查询产品预警库存信息分页列表
     *
     * @param page 分页参数
     * @param warehouseId 仓库ID
     * @param deptId 部门ID
     * @param name 产品名称
     * @return
     */
    IPage<ProductStockWarningVO> listWarningStockPage(Page page, @Param("warehouseId") Long warehouseId, @Param("deptId") Long deptId, @Param("name") String name);
}

