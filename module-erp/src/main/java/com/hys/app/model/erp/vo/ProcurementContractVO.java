package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProcurementContract;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 采购合同实体VO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementContractVO extends ProcurementContract implements Serializable {

    private static final long serialVersionUID = -1901787577013351330L;

    /**
     * 合同商品
     */
    @ApiModelProperty(name = "product_list", value = "合同商品")
    private List<ProcurementContractProduct> productList;

    public List<ProcurementContractProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ProcurementContractProduct> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "ProcurementContractVO{" +
                "productList=" + productList +
                '}';
    }
}
