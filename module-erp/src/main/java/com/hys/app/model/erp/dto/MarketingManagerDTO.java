package com.hys.app.model.erp.dto;

import com.hys.app.framework.validation.annotation.Mobile;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 营销经理新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@Data
public class MarketingManagerDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "real_name", value = "真实姓名")
    @NotBlank(message = "真实姓名不能为空")
    private String realName;

    @ApiModelProperty(name = "mobile", value = "电话")
    @NotBlank(message = "电话不能为空")
    @Mobile
    private String mobile;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    @NotNull(message = "部门id不能为空")
    private Long deptId;

    @ApiModelProperty(name = "disable_flag", value = "0正常 1禁用")
    @NotNull(message = "状态不能为空")
    private Integer disableFlag;


}

