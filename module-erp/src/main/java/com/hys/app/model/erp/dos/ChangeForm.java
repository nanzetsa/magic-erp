package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.vo.ChangeFormAllowable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 换货单实体
 * @author dmy
 * 2023-12-05
 */
@TableName("es_change_form")
@ApiModel
public class ChangeForm implements Serializable {

    private static final long serialVersionUID = -7086970733393079349L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 订单编号
     */
    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;
    /**
     * 服务专员ID
     */
    @ApiModelProperty(name = "staff_id", value = "服务专员ID")
    private Long staffId;
    /**
     * 服务专员名称
     */
    @ApiModelProperty(name = "staff_name", value = "服务专员名称")
    private String staffName;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    /**
     * 换货金额
     */
    @ApiModelProperty(name = "change_amount", value = "换货金额")
    private Double changeAmount;
    /**
     * 经手人ID
     */
    @ApiModelProperty(name = "handled_by_id", value = "经手人ID")
    private Long handledById;
    /**
     * 经手人
     */
    @ApiModelProperty(name = "handled_by", value = "经手人")
    private String handledBy;
    /**
     * 换货说明
     */
    @ApiModelProperty(name = "change_desc", value = "换货说明")
    private String changeDesc;
    /**
     * 换货时间
     */
    @ApiModelProperty(name = "change_time", value = "换货时间")
    private Long changeTime;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 状态
     * @see com.hys.app.model.erp.enums.ChangeFormStatusEnum
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    /**
     * 审核驳回原因
     */
    @ApiModelProperty(name = "reject_reason", value = "审核驳回原因")
    private String rejectReason;
    /**
     * 提交审核时间
     */
    @ApiModelProperty(name = "submit_time", value = "提交审核时间")
    private Long submitTime;
    /**
     * 审核时间
     */
    @ApiModelProperty(name = "audit_time", value = "审核时间")
    private Long auditTime;
    /**
     * 审核人ID
     */
    @ApiModelProperty(name = "audit_person_id", value = "审核人ID")
    private Long auditPersonId;
    /**
     * 审核人名称
     */
    @ApiModelProperty(name = "audit_person_name", value = "审核人名称")
    private String auditPersonName;
    /**
     * 换货单允许进行的操作
     */
    @TableField(exist = false)
    @ApiModelProperty(name = "allowable", value = "换货单允许进行的操作")
    private ChangeFormAllowable allowable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Double getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(Double changeAmount) {
        this.changeAmount = changeAmount;
    }

    public Long getHandledById() {
        return handledById;
    }

    public void setHandledById(Long handledById) {
        this.handledById = handledById;
    }

    public String getHandledBy() {
        return handledBy;
    }

    public void setHandledBy(String handledBy) {
        this.handledBy = handledBy;
    }

    public String getChangeDesc() {
        return changeDesc;
    }

    public void setChangeDesc(String changeDesc) {
        this.changeDesc = changeDesc;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Long getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Long submitTime) {
        this.submitTime = submitTime;
    }

    public Long getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Long auditTime) {
        this.auditTime = auditTime;
    }

    public Long getAuditPersonId() {
        return auditPersonId;
    }

    public void setAuditPersonId(Long auditPersonId) {
        this.auditPersonId = auditPersonId;
    }

    public String getAuditPersonName() {
        return auditPersonName;
    }

    public void setAuditPersonName(String auditPersonName) {
        this.auditPersonName = auditPersonName;
    }

    public ChangeFormAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(ChangeFormAllowable allowable) {
        this.allowable = allowable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChangeForm that = (ChangeForm) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sn, that.sn) &&
                Objects.equals(orderSn, that.orderSn) &&
                Objects.equals(staffId, that.staffId) &&
                Objects.equals(staffName, that.staffName) &&
                Objects.equals(deptId, that.deptId) &&
                Objects.equals(deptName, that.deptName) &&
                Objects.equals(warehouseId, that.warehouseId) &&
                Objects.equals(warehouseName, that.warehouseName) &&
                Objects.equals(changeAmount, that.changeAmount) &&
                Objects.equals(handledById, that.handledById) &&
                Objects.equals(handledBy, that.handledBy) &&
                Objects.equals(changeDesc, that.changeDesc) &&
                Objects.equals(changeTime, that.changeTime) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(status, that.status) &&
                Objects.equals(rejectReason, that.rejectReason) &&
                Objects.equals(submitTime, that.submitTime) &&
                Objects.equals(auditTime, that.auditTime) &&
                Objects.equals(auditPersonId, that.auditPersonId) &&
                Objects.equals(auditPersonName, that.auditPersonName) &&
                Objects.equals(allowable, that.allowable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, orderSn, staffId, staffName, deptId, deptName, warehouseId, warehouseName, changeAmount, handledById, handledBy, changeDesc, changeTime, createTime, status, rejectReason, submitTime, auditTime, auditPersonId, auditPersonName, allowable);
    }

    @Override
    public String toString() {
        return "ChangeForm{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", orderSn='" + orderSn + '\'' +
                ", staffId=" + staffId +
                ", staffName='" + staffName + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", changeAmount=" + changeAmount +
                ", handledById=" + handledById +
                ", handledBy='" + handledBy + '\'' +
                ", changeDesc='" + changeDesc + '\'' +
                ", changeTime=" + changeTime +
                ", createTime=" + createTime +
                ", status='" + status + '\'' +
                ", rejectReason='" + rejectReason + '\'' +
                ", submitTime=" + submitTime +
                ", auditTime=" + auditTime +
                ", auditPersonId=" + auditPersonId +
                ", auditPersonName='" + auditPersonName + '\'' +
                ", allowable=" + allowable +
                '}';
    }
}
