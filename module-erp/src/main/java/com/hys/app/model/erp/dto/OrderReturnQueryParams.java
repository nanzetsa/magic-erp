package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单退货查询参数
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderReturnQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "sn", value = "退货单编号")
    private String sn;
    
    @ApiModelProperty(name = "order_sn", value = "要退货的订单")
    private String orderSn;

    @ApiModelProperty(name = "order_type", value = "要退货的订单类型")
    private String orderType;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "distribution_name", value = "销售经理")
    private String distributionName;
    
    @ApiModelProperty(name = "handle_by", value = "经手人")
    private String handleBy;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

}

