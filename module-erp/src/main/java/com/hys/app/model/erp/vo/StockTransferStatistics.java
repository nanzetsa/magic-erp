package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StockTransferProductDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存调拨统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockTransferStatistics extends StockTransferProductDO implements Serializable {

    private static final long serialVersionUID = -7352316619178134278L;

    @ApiModelProperty(name = "sn", value = "调拨单编号")
    private String sn;

    @ApiModelProperty(name = "transfer_time", value = "调拨时间")
    private Long transferTime;

    @ApiModelProperty(name = "out_warehouse_name", value = "调出方仓库")
    private String outWarehouseName;

    @ApiModelProperty(name = "in_warehouse_name", value = "调入方仓库")
    private String inWarehouseName;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getTransferTime() {
        return transferTime;
    }

    public void setTransferTime(Long transferTime) {
        this.transferTime = transferTime;
    }

    public String getOutWarehouseName() {
        return outWarehouseName;
    }

    public void setOutWarehouseName(String outWarehouseName) {
        this.outWarehouseName = outWarehouseName;
    }

    public String getInWarehouseName() {
        return inWarehouseName;
    }

    public void setInWarehouseName(String inWarehouseName) {
        this.inWarehouseName = inWarehouseName;
    }

    @Override
    public String toString() {
        return "StockTransferStatistics{" +
                "sn='" + sn + '\'' +
                ", transferTime=" + transferTime +
                ", outWarehouseName='" + outWarehouseName + '\'' +
                ", inWarehouseName='" + inWarehouseName + '\'' +
                '}';
    }
}
