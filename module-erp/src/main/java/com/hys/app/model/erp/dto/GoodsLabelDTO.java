package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 商品标签 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Data
@ApiModel(value = "商品标签 新增|编辑 请求参数")
public class GoodsLabelDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "标签名称")
    @NotBlank(message = "标签名称不能为空")
    private String name;
    
}

