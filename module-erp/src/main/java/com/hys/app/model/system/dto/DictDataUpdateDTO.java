package com.hys.app.model.system.dto;

import com.hys.app.model.system.vo.DictDataBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@ApiModel(value = "管理后台 - 字典数据更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class DictDataUpdateDTO extends DictDataBaseVO {

    @ApiModelProperty(value = "字典数据编号", example = "1024")
    @NotNull(message = "字典数据编号不能为空")
    private Long id;

}
