package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 营销经理查询参数
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MarketingManagerQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字搜索")
    private String keyword;

    @ApiModelProperty(value = "地区id", name = "region_id")
    private Long regionId;

    @ApiModelProperty(name = "member_id", value = "会员id")
    private Long memberId;

    @ApiModelProperty(name = "member_name", value = "会员名称")
    private String memberName;
    
    @ApiModelProperty(name = "province_id", value = "所属省份ID")
    private Long provinceId;
    
    @ApiModelProperty(name = "city_id", value = "所属城市ID")
    private Long cityId;
    
    @ApiModelProperty(name = "county_id", value = "所属县(区)ID")
    private Long countyId;
    
    @ApiModelProperty(name = "town_id", value = "所属城镇ID")
    private Long townId;
    
    @ApiModelProperty(name = "province", value = "所属省份名称")
    private String province;
    
    @ApiModelProperty(name = "city", value = "所属城市名称")
    private String city;
    
    @ApiModelProperty(name = "county", value = "所属县(区)名称")
    private String county;
    
    @ApiModelProperty(name = "town", value = "所属城镇名称")
    private String town;
    
    @ApiModelProperty(name = "disable_flag", value = "0正常 1禁用")
    private Integer disableFlag;
    
    @ApiModelProperty(name = "region_path", value = "地区path")
    private String regionPath;
    
    @ApiModelProperty(name = "real_name", value = "真实姓名")
    private String realName;
    
    @ApiModelProperty(name = "mobile", value = "电话")
    private String mobile;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
}

