package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 收款账户 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-01-24 14:43:18
 */
@Data
@ApiModel(value = "收款账户 新增|编辑 请求参数")
public class CollectingAccountDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "账户名称")
    @NotBlank(message = "账户名称不能为空")
    private String name;

    @ApiModelProperty(name = "default_flag", value = "是否默认")
    @NotNull(message = "是否默认不能为空")
    private Boolean defaultFlag;

    @ApiModelProperty(name = "enable_flag", value = "是否启用")
    @NotNull(message = "是否启用不能为空")
    private Boolean enableFlag;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}

