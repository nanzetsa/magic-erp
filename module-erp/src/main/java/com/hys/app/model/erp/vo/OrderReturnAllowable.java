package com.hys.app.model.erp.vo;

import com.hys.app.framework.context.user.AdminUserContext;
import com.hys.app.model.erp.dos.OrderReturnDO;
import com.hys.app.model.erp.enums.OrderReturnStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * 订单退货操作
 *
 * @author 张崧
 * @since 2023-12-15
 */
@Data
@NoArgsConstructor
public class OrderReturnAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "submit", value = "是否允许提交")
    private Boolean submit;

    @ApiModelProperty(name = "withdraw", value = "是否允许撤回")
    private Boolean withdraw;

    @ApiModelProperty(name = "audit", value = "是否允许审核")
    private Boolean audit;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;

    public OrderReturnAllowable(OrderReturnDO orderReturnDO) {
        OrderReturnStatusEnum status = orderReturnDO.getStatus();
        if (status == null) {
            return;
        }

        this.setEdit(status == OrderReturnStatusEnum.NotSubmit ||
                status == OrderReturnStatusEnum.AuditReject);

        this.setSubmit(status == OrderReturnStatusEnum.NotSubmit ||
                status == OrderReturnStatusEnum.AuditReject);

        this.setDelete(status == OrderReturnStatusEnum.NotSubmit ||
                status == OrderReturnStatusEnum.AuditReject);

        this.setWithdraw(status == OrderReturnStatusEnum.Submit &&
                Objects.equals(AdminUserContext.getAdminUserName(), orderReturnDO.getCreator()));

        this.setAudit(status == OrderReturnStatusEnum.Submit);
    }

}

