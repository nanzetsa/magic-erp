package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 产品查询参数
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "sn", value = "产品编号")
    private String sn;

    @ApiModelProperty(name = "name", value = "产品名称")
    private String name;

    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;

    @ApiModelProperty(name = "specification", value = "规格")
    private String specification;

    @ApiModelProperty(name = "unit", value = "单位")
    private String unit;

    @ApiModelProperty(name = "warehouse_id", value = "查询哪个仓库的库存")
    private Long warehouseId;

    @ApiModelProperty(name = "market_enable", value = "是否上架 true上架")
    private Boolean marketEnable;

}

