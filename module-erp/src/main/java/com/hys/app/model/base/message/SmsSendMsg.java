package com.hys.app.model.base.message;

import com.hys.app.model.system.enums.SmsServiceCodeEnum;

import java.io.Serializable;
import java.util.Map;

/**
 * 发送短信
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-02-05
 */
public class SmsSendMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 652862137182690060L;

	/**
	 * 手机号
	 */
	private String phoneNumber;
	/**
	 * 业务类型编号
	 */
	private SmsServiceCodeEnum serviceCode;
	/**
	 * 模板变量
	 */
	private Map<String, Object> valuesMap;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public SmsServiceCodeEnum getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(SmsServiceCodeEnum serviceCode) {
		this.serviceCode = serviceCode;
	}

	public Map<String, Object> getValuesMap() {
		return valuesMap;
	}

	public void setValuesMap(Map<String, Object> valuesMap) {
		this.valuesMap = valuesMap;
	}
}
