package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 订单支付明细excel导出
 *
 * @author 张崧
 * 2024-01-24 17:00:32
 */
@Data
public class OrderPaymentExcelVO {

    @ExcelProperty("订单id")
    private Long orderId;
    
    @ExcelProperty("收款账户id")
    private Long collectingAccountId;
    
    @ExcelProperty("收款账户名称")
    private String collectingAccountName;
    
    @ExcelProperty("支付金额")
    private Double price;
    
    @ExcelProperty("备注")
    private String remark;
    
}

