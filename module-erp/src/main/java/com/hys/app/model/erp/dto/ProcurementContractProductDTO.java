package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 采购合同产品实体DTO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementContractProductDTO implements Serializable {

    private static final long serialVersionUID = -1165641581078962814L;

    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID", required = true)
    @NotNull(message = "商品ID不能为空")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称", required = true)
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号", required = true)
    @NotEmpty(message = "产品编号不能为空")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类ID
     */
    @ApiModelProperty(name = "category_id", value = "分类ID", required = true)
    @NotNull(message = "分类ID不能为空")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = true)
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位", required = true)
    @NotEmpty(message = "产品单位不能为空")
    private String unit;
    /**
     * 条形码
     */
    @ApiModelProperty(name = "barcode", value = "条形码", required = true)
    @NotEmpty(message = "条形码不能为空")
    private String barcode;
    /**
     * 单价
     */
    @ApiModelProperty(name = "price", value = "单价", required = true)
    @NotNull(message = "单价不能为空")
    private Double price;
    /**
     * 数量
     */
    @ApiModelProperty(name = "num", value = "数量", required = true)
    @NotNull(message = "数量不能为空")
    private Integer num;
    /**
     * 税率
     */
    @ApiModelProperty(name = "tax_rate", value = "税率", required = true)
    @NotNull(message = "税率不能为空")
    private Double taxRate;
    /**
     * 合价
     */
    @ApiModelProperty(name = "total_price", value = "合价", required = true)
    @NotNull(message = "合价不能为空")
    private Double totalPrice;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ProcurementContractProductDTO{" +
                "goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", barcode='" + barcode + '\'' +
                ", price=" + price +
                ", num=" + num +
                ", taxRate=" + taxRate +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
