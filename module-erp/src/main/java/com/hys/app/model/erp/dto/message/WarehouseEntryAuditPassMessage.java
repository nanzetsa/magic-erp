package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.WarehouseEntryDO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 入库单审核通过消息
 *
 * @author 张崧
 * @since 2024-01-08
 */
@Data
public class WarehouseEntryAuditPassMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    /**
     * 入库单列表
     */
    private List<WarehouseEntryDO> list;

}
