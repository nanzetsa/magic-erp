package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 商品借出单归还实体DTO
 *
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormReturnDTO implements Serializable {

    private static final long serialVersionUID = -640454945651941974L;

    /**
     * 归还人ID
     */
    @ApiModelProperty(name = "return_person_id", value = "归还人ID", required = true)
    @NotNull(message = "归还人ID不能为空")
    private Long returnPersonId;
    /**
     * 归还人
     */
    @ApiModelProperty(name = "return_person", value = "归还人", required = true)
    @NotEmpty(message = "归还人不能为空")
    private String returnPerson;
    /**
     * 归还登记人ID
     */
    @ApiModelProperty(name = "return_register_id", value = "归还登记人ID", required = true)
    @NotNull(message = "归还登记人ID不能为空")
    private Long returnRegisterId;
    /**
     * 归还登记人
     */
    @ApiModelProperty(name = "return_register", value = "归还登记人", required = true)
    @NotEmpty(message = "归还登记人不能为空")
    private String returnRegister;
    /**
     * 归还时间
     */
    @ApiModelProperty(name = "return_time", value = "归还时间", required = true)
    @NotNull(message = "归还时间不能为空")
    private Long returnTime;
    /**
     * 归还说明
     */
    @ApiModelProperty(name = "return_desc", value = "归还说明", required = true)
    @NotEmpty(message = "归还说明不能为空")
    private String returnDesc;
    /**
     * 借出单产品归还数量信息
     */
    @ApiModelProperty(name = "product_list", value = "借出单产品归还数量信息", required = true)
    @NotNull(message = "借出单产品归还数量信息不能为空")
    private List<LendFormProductReturnDTO> productList;

    public Long getReturnPersonId() {
        return returnPersonId;
    }

    public void setReturnPersonId(Long returnPersonId) {
        this.returnPersonId = returnPersonId;
    }

    public String getReturnPerson() {
        return returnPerson;
    }

    public void setReturnPerson(String returnPerson) {
        this.returnPerson = returnPerson;
    }

    public Long getReturnRegisterId() {
        return returnRegisterId;
    }

    public void setReturnRegisterId(Long returnRegisterId) {
        this.returnRegisterId = returnRegisterId;
    }

    public String getReturnRegister() {
        return returnRegister;
    }

    public void setReturnRegister(String returnRegister) {
        this.returnRegister = returnRegister;
    }

    public Long getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Long returnTime) {
        this.returnTime = returnTime;
    }

    public String getReturnDesc() {
        return returnDesc;
    }

    public void setReturnDesc(String returnDesc) {
        this.returnDesc = returnDesc;
    }

    public List<LendFormProductReturnDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<LendFormProductReturnDTO> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "LendFormReturnDTO{" +
                "returnPersonId=" + returnPersonId +
                ", returnPerson='" + returnPerson + '\'' +
                ", returnRegisterId=" + returnRegisterId +
                ", returnRegister='" + returnRegister + '\'' +
                ", returnTime=" + returnTime +
                ", returnDesc='" + returnDesc + '\'' +
                ", productList=" + productList +
                '}';
    }
}
