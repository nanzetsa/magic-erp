package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 库存盘点单产品实体DTO
 *
 * @author dmy
 * 2023-12-05
 */
public class StockInventoryProductDTO implements Serializable {

    private static final long serialVersionUID = -6285460490304810323L;

    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID", required = true)
    @NotNull(message = "商品ID不能为空")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID", required = true)
    @NotNull(message = "产品ID不能为空")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称", required = true)
    @NotEmpty(message = "产品名称不能为空")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号", required = true)
    @NotEmpty(message = "产品编号不能为空")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格", required = true)
    @NotEmpty(message = "产品规格不能为空")
    private String specification;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称", required = true)
    @NotEmpty(message = "分类名称不能为空")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位", required = true)
    @NotEmpty(message = "产品单位不能为空")
    private String unit;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量", required = true)
    @NotNull(message = "库存数量不能为空")
    private Integer stockNum;
    /**
     * 盘点数量
     */
    @ApiModelProperty(name = "inventory_num", value = "盘点数量", required = true)
    @NotNull(message = "盘点数量不能为空")
    private Integer inventoryNum;
    /**
     * 差异数量
     */
    @ApiModelProperty(name = "diff_num", value = "差异数量", required = true)
    @NotNull(message = "差异数量不能为空")
    private Integer diffNum;
    /**
     * 备注
     */
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(Integer inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    public Integer getDiffNum() {
        return diffNum;
    }

    public void setDiffNum(Integer diffNum) {
        this.diffNum = diffNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "StockInventoryProductDTO{" +
                "goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", stockNum=" + stockNum +
                ", inventoryNum=" + inventoryNum +
                ", diffNum=" + diffNum +
                ", remark='" + remark + '\'' +
                '}';
    }
}
