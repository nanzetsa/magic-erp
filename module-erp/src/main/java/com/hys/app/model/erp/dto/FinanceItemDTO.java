package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.enums.FinanceAmountTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 财务明细 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "财务明细 新增|编辑 请求参数")
public class FinanceItemDTO {

    @ApiModelProperty(name = "source_sn", value = "来源")
    @NotBlank(message = "来源不能为空")
    private String sourceType;

    @ApiModelProperty(name = "source_sn", value = "来源编号")
    private String sourceSn;

    @ApiModelProperty(name = "amount", value = "金额")
    @NotNull(message = "金额不能为空")
    @DecimalMin(value = "0.01", message = "金额必须大于0")
    private Double amount;

    @ApiModelProperty(name = "amount_type", value = "收入/支出")
    @NotNull(message = "收入/支出不能为空")
    private FinanceAmountTypeEnum amountType;

}

