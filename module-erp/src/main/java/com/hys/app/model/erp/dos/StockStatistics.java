package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 库存统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@TableName("es_stock_statistics")
@ApiModel
public class StockStatistics implements Serializable {

    private static final long serialVersionUID = 2623823469276813564L;
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号")
    private String stockSn;
    /**
     * 批次ID
     */
    @ApiModelProperty(name = "batch_id", value = "批次ID")
    private Long batchId;
    /**
     * 批次编号
     */
    @ApiModelProperty(name = "batch_sn", value = "批次编号")
    private String batchSn;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类ID
     */
    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 入库数量
     */
    @ApiModelProperty(name = "in_num", value = "入库数量")
    private Integer inNum;
    /**
     * 出库数量
     */
    @ApiModelProperty(name = "out_num", value = "出库数量")
    private Integer outNum;
    /**
     * 供应商退货数量
     */
    @ApiModelProperty(name = "supplier_return_num", value = "供应商退货数量")
    private Integer supplierReturnNum;
    /**
     * 订单退货数量
     */
    @ApiModelProperty(name = "order_return_num", value = "订单退货数量")
    private Integer orderReturnNum;
    /**
     * 调出数量
     */
    @ApiModelProperty(name = "transfer_out_num", value = "调出数量")
    private Integer transferOutNum;
    /**
     * 调入数量
     */
    @ApiModelProperty(name = "transfer_int_num", value = "调入数量")
    private Integer transferInNum;
    /**
     * 库存调整(报损)数量
     */
    @ApiModelProperty(name = "damage_num", value = "库存调整(报损)数量")
    private Integer damageNum;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;
    /**
     * 预警数量
     */
    @ApiModelProperty(name = "warning_num", value = "预警数量")
    private Integer warningNum;
    /**
     * 剩余库存数量
     */
    @ApiModelProperty(name = "remain_num", value = "剩余库存数量")
    private Integer remainNum;
    /**
     * 换货数量
     */
    @ApiModelProperty(name = "change_num", value = "换货数量")
    private Integer changeNum;
    /**
     * 零售出库数量
     */
    @ApiModelProperty(name = "retail_order_num", value = "零售出库数量")
    private Integer retailOrderNum;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getBatchSn() {
        return batchSn;
    }

    public void setBatchSn(String batchSn) {
        this.batchSn = batchSn;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getInNum() {
        return inNum;
    }

    public void setInNum(Integer inNum) {
        this.inNum = inNum;
    }

    public Integer getOutNum() {
        return outNum;
    }

    public void setOutNum(Integer outNum) {
        this.outNum = outNum;
    }

    public Integer getSupplierReturnNum() {
        return supplierReturnNum;
    }

    public void setSupplierReturnNum(Integer supplierReturnNum) {
        this.supplierReturnNum = supplierReturnNum;
    }

    public Integer getOrderReturnNum() {
        return orderReturnNum;
    }

    public void setOrderReturnNum(Integer orderReturnNum) {
        this.orderReturnNum = orderReturnNum;
    }

    public Integer getTransferOutNum() {
        return transferOutNum;
    }

    public void setTransferOutNum(Integer transferOutNum) {
        this.transferOutNum = transferOutNum;
    }

    public Integer getTransferInNum() {
        return transferInNum;
    }

    public void setTransferInNum(Integer transferInNum) {
        this.transferInNum = transferInNum;
    }

    public Integer getDamageNum() {
        return damageNum;
    }

    public void setDamageNum(Integer damageNum) {
        this.damageNum = damageNum;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getWarningNum() {
        return warningNum;
    }

    public void setWarningNum(Integer warningNum) {
        this.warningNum = warningNum;
    }

    public Integer getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(Integer remainNum) {
        this.remainNum = remainNum;
    }

    public Integer getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(Integer changeNum) {
        this.changeNum = changeNum;
    }

    public Integer getRetailOrderNum() {
        return retailOrderNum;
    }

    public void setRetailOrderNum(Integer retailOrderNum) {
        this.retailOrderNum = retailOrderNum;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockStatistics that = (StockStatistics) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(deptId, that.deptId) &&
                Objects.equals(deptName, that.deptName) &&
                Objects.equals(warehouseId, that.warehouseId) &&
                Objects.equals(warehouseName, that.warehouseName) &&
                Objects.equals(stockSn, that.stockSn) &&
                Objects.equals(batchId, that.batchId) &&
                Objects.equals(batchSn, that.batchSn) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSn, that.productSn) &&
                Objects.equals(specification, that.specification) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(inNum, that.inNum) &&
                Objects.equals(outNum, that.outNum) &&
                Objects.equals(supplierReturnNum, that.supplierReturnNum) &&
                Objects.equals(orderReturnNum, that.orderReturnNum) &&
                Objects.equals(transferOutNum, that.transferOutNum) &&
                Objects.equals(transferInNum, that.transferInNum) &&
                Objects.equals(damageNum, that.damageNum) &&
                Objects.equals(stockNum, that.stockNum) &&
                Objects.equals(warningNum, that.warningNum) &&
                Objects.equals(remainNum, that.remainNum) &&
                Objects.equals(changeNum, that.changeNum) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, deptId, deptName, warehouseId, warehouseName, stockSn, batchId, batchSn, productId, productName, productSn, specification, categoryId, categoryName, unit, inNum, outNum, supplierReturnNum, orderReturnNum, transferOutNum, transferInNum, damageNum, stockNum, warningNum, remainNum, changeNum, createTime);
    }

    @Override
    public String toString() {
        return "StockStatistics{" +
                "id=" + id +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", stockSn='" + stockSn + '\'' +
                ", batchId=" + batchId +
                ", batchSn='" + batchSn + '\'' +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", inNum=" + inNum +
                ", outNum=" + outNum +
                ", supplierReturnNum=" + supplierReturnNum +
                ", orderReturnNum=" + orderReturnNum +
                ", transferOutNum=" + transferOutNum +
                ", transferInNum=" + transferInNum +
                ", damageNum=" + damageNum +
                ", stockNum=" + stockNum +
                ", warningNum=" + warningNum +
                ", remainNum=" + remainNum +
                ", changeNum=" + changeNum +
                ", createTime=" + createTime +
                '}';
    }
}
