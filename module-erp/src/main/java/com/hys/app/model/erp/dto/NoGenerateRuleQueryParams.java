package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 编号生成规则查询参数
 *
 * @author 张崧
 * 2023-12-01 11:37:56
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NoGenerateRuleQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "type", value = "业务类型")
    private String type;
    
    @ApiModelProperty(name = "name", value = "规则名称")
    private String name;
    
    @ApiModelProperty(name = "seq_generate_type", value = "顺序号的重置规则")
    private String seqGenerateType;
    
    @ApiModelProperty(name = "seq_begin_number", value = "顺序号的起始值")
    private Long seqBeginNumber;
    
    @ApiModelProperty(name = "open_flag", value = "是否启用该规则")
    private Boolean openFlag;
    
    @ApiModelProperty(name = "item_list", value = "规则项列表")
    private String itemList;
    
}

