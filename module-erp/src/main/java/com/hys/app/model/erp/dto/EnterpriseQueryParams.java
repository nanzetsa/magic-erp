package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 企业分页查询参数
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "企业分页查询参数")
public class EnterpriseQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "name", value = "企业名称")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "mobile", value = "手机号")
    private String mobile;
    
    @ApiModelProperty(name = "email", value = "邮箱")
    private String email;
    
    @ApiModelProperty(name = "zip_code", value = "邮编")
    private String zipCode;
    
    @ApiModelProperty(name = "tax_num", value = "纳税人识别号")
    private String taxNum;
    
    @ApiModelProperty(name = "bank_name", value = "开户银行")
    private String bankName;
    
    @ApiModelProperty(name = "address", value = "地址")
    private String address;
    
    @ApiModelProperty(name = "linkman", value = "联系人")
    private String linkman;
    
    @ApiModelProperty(name = "link_phone", value = "联系电话")
    private String linkPhone;
    
    @ApiModelProperty(name = "fax", value = "传真")
    private String fax;
    
    @ApiModelProperty(name = "bank_account", value = "银行账号")
    private String bankAccount;
    
    @ApiModelProperty(name = "bank_account_name", value = "银行户名")
    private String bankAccountName;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

