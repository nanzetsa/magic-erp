package com.hys.app.model.system.dos;

import com.baomidou.mybatisplus.annotation.*;
import com.hys.app.model.system.enums.AuditStatusEnum;
import com.hys.app.model.system.enums.MessageCodeEnum;
import com.hys.app.model.system.enums.SmsTemplateServiceTypeEnum;
import com.hys.app.model.system.enums.SmsTemplateTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


/**
 * 短信模板实体
 *
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-01-26 16:04:22
 */
@TableName("es_sms_template")
@ApiModel
public class SmsTemplate implements Serializable {

    private static final long serialVersionUID = 6526154401398092L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * es_sms_platform表的beanid
     */
    @ApiModelProperty(name = "bean_id", value = "es_sms_platform表的beanid")
    private String beanId;
    /**
     * 模板业务类型
     *
     * @see SmsTemplateServiceTypeEnum
     */
    @ApiModelProperty(name = "service_type", value = "模板业务类型")
    private String serviceType;
    /**
     * 模板业务编号
     *
     * @see MessageCodeEnum
     */
    @ApiModelProperty(name = "service_code", value = "模板业务编号")
    private String serviceCode;
    /**
     * 模板名称
     */
    @ApiModelProperty(name = "template_name", value = "模板名称")
    private String templateName;
    /**
     * 模板内容
     */
    @NotEmpty(message = "模板内容不能为空")
    @ApiModelProperty(name = "template_content", value = "模板内容", required = true)
    private String templateContent;
    /**
     * 短信模板code(第三方返回的)
     */
    @ApiModelProperty(name = "template_code", value = "短信模板code(第三方返回的)")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String templateCode;
    /**
     * 短信类型
     *
     * @see SmsTemplateTypeEnum
     */
    @NotEmpty(message = "短信类型不能为空")
    @ApiModelProperty(name = "template_type", value = "短信类型")
    private String templateType;
    /**
     * 申请说明
     */
    @ApiModelProperty(name = "remark", value = "申请说明")
    private String remark;
    /**
     * 短信模板审核工单号(第三方返回的)
     */
    @ApiModelProperty(name = "order_id", value = "短信模板审核工单号(第三方返回的)")
    private String orderId;
    /**
     * 模板审核状态
     *
     * @see AuditStatusEnum
     */
    @ApiModelProperty(name = "audit_status", value = "模板审核状态")
    private String auditStatus;
    /**
     * 启用状态
     */
    @ApiModelProperty(name = "enable_status", value = "启用状态")
    private String enableStatus;
    /**
     * 审核未通过原因
     */
    @ApiModelProperty(name = "fail_reason", value = "审核未通过原因")
    private String failReason;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBeanId() {
        return beanId;
    }

    public void setBeanId(String beanId) {
        this.beanId = beanId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(String enableStatus) {
        this.enableStatus = enableStatus;
    }

    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SmsTemplate that = (SmsTemplate) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(beanId, that.beanId)
                .append(serviceType, that.serviceType)
                .append(serviceCode, that.serviceCode)
                .append(templateName, that.templateName)
                .append(templateContent, that.templateContent)
                .append(templateCode, that.templateCode)
                .append(templateType, that.templateType)
                .append(remark, that.remark)
                .append(orderId, that.orderId)
                .append(auditStatus, that.auditStatus)
                .append(enableStatus, that.enableStatus)
                .append(failReason, that.failReason)
                .append(createTime, that.createTime)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(beanId)
                .append(serviceType)
                .append(serviceCode)
                .append(templateName)
                .append(templateContent)
                .append(templateCode)
                .append(templateType)
                .append(remark)
                .append(orderId)
                .append(auditStatus)
                .append(enableStatus)
                .append(failReason)
                .append(createTime)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "SmsTemplate{" +
                "id=" + id +
                ", beanId='" + beanId + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", serviceCode='" + serviceCode + '\'' +
                ", templateName='" + templateName + '\'' +
                ", templateContent='" + templateContent + '\'' +
                ", templateCode='" + templateCode + '\'' +
                ", templateType='" + templateType + '\'' +
                ", remark='" + remark + '\'' +
                ", orderId='" + orderId + '\'' +
                ", auditStatus='" + auditStatus + '\'' +
                ", enableStatus='" + enableStatus + '\'' +
                ", failReason='" + failReason + '\'' +
                ", createTime=" + createTime +
                '}';
    }


}