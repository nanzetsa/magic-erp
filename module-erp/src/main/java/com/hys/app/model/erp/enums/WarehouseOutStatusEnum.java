package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 出库单状态枚举
 *
 * @author 张崧
 * @since 2023-12-07
 */
@Getter
@AllArgsConstructor
public enum WarehouseOutStatusEnum {

    /**
     * 待审核
     */
    WAIT_AUDIT,
    /**
     * 审核通过
     */
    AUDIT_PASS,
    /**
     * 审核驳回
     */
    AUDIT_REJECT,
    /**
     * 已发货
     */
    SHIP,

}
