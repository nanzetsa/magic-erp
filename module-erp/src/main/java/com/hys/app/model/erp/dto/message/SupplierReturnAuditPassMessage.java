package com.hys.app.model.erp.dto.message;

import com.hys.app.model.erp.dos.SupplierReturnDO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 供应商退货单审核通过消息
 *
 * @author 张崧
 * @since 2024-01-08
 */
@Data
public class SupplierReturnAuditPassMessage implements Serializable {

    private static final long serialVersionUID = 524607318299159659L;

    private List<SupplierReturnDO> list;

}
