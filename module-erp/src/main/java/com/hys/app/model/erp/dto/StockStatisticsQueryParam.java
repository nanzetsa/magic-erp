package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 库存统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class StockStatisticsQueryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = -7796069338813093018L;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "stock_sn", value = "入库单编号")
    private String stockSn;

    @ApiModelProperty(name = "group_by_warehouse_entry", value = "是否按入库单分组", hidden = true)
    @JsonIgnore
    private Boolean groupByWarehouseEntry;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Boolean getGroupByWarehouseEntry() {
        return groupByWarehouseEntry;
    }

    public void setGroupByWarehouseEntry(Boolean groupByWarehouseEntry) {
        this.groupByWarehouseEntry = groupByWarehouseEntry;
    }

    @Override
    public String toString() {
        return "StockStatisticsQueryParam{" +
                "deptId=" + deptId +
                ", warehouseId=" + warehouseId +
                ", categoryId=" + categoryId +
                ", productName='" + productName + '\'' +
                ", stockSn='" + stockSn + '\'' +
                '}';
    }
}
