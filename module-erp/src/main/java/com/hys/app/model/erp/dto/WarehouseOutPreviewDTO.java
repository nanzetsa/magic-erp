package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 出库单预览DTO
 *
 * @author 张崧
 * @since 2023-12-11
 */
@Data
public class WarehouseOutPreviewDTO {

    @ApiModelProperty(name = "order_id_list", value = "订单id列表")
    @NotEmpty(message = "订单id不能为空")
    private List<Long> orderIdList;

}

