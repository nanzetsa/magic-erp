package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.enums.StockOperateEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 库存更新
 *
 * @author 张崧
 * @since 2024-01-15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockUpdateDTO {

    @ApiModelProperty(name = "batch_id", value = "批次id")
    private Long batchId;

    @ApiModelProperty(name = "tip", value = "库存不足时的提示")
    private String tip;

    @ApiModelProperty(name = "operate", value = "增加/减少")
    private StockOperateEnum operate;

    @ApiModelProperty(name = "change_num", value = "变化库存数量，必须大于0")
    private Integer changeNum;

}

