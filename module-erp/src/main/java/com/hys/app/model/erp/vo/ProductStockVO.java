package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProductStockDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品库存详情
 *
 * @author 张崧
 * @since 2023-12-07 10:08:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductStockVO extends ProductStockDO {

}

