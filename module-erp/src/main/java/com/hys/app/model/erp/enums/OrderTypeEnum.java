package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单类型枚举
 *
 * @author zs
 * @since 2024-03-29
 */
@Getter
@AllArgsConstructor
public enum OrderTypeEnum {

    /**
     * 零售订单
     */
    TO_C("零售订单"),
    /**
     * 企业销售订单
     */
    TO_B("企业销售订单");

    private final String text;

}
