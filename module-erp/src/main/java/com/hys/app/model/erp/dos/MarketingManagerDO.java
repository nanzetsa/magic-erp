package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 营销经理实体类
 *
 * @author 张崧
 * @since 2023-12-11 11:25:31
 */
@TableName("erp_marketing_manager")
@Data
public class MarketingManagerDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "real_name", value = "真实姓名")
    private String realName;

    @ApiModelProperty(name = "mobile", value = "电话")
    private String mobile;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @ApiModelProperty(name = "disable_flag", value = "0正常 1禁用")
    private Integer disableFlag;

}
