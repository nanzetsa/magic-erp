package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 库存报损单产品实体
 *
 * @author dmy
 * 2023-12-05
 */
@TableName("es_stock_damage_report_product")
@ApiModel
public class StockDamageReportProduct implements Serializable {

    private static final long serialVersionUID = -3018714217635930969L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 报损单ID
     */
    @ApiModelProperty(name = "report_id", value = "报损单ID")
    private Long reportId;
    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号")
    private String stockSn;
    /**
     * 批次ID
     */
    @ApiModelProperty(name = "batch_id", value = "批次ID")
    private Long batchId;
    /**
     * 批次编号
     */
    @ApiModelProperty(name = "batch_sn", value = "批次编号")
    private String batchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类ID
     */
    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 单价
     */
    @ApiModelProperty(name = "price", value = "单价")
    private Double price;
    /**
     * 成本价
     */
    @ApiModelProperty(name = "cost_price", value = "成本价")
    private Double costPrice;
    /**
     * 税率
     */
    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;
    /**
     * 报损数量
     */
    @ApiModelProperty(name = "report_num", value = "报损数量")
    private Integer reportNum;
    /**
     * 备注
     */
    @ApiModelProperty(name = "report_remark", value = "备注")
    private String reportRemark;
    /**
     * 类型  0：增加，1：减少
     */
    @ApiModelProperty(name = "type", value = "类型 0：增加，1：减少")
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getBatchSn() {
        return batchSn;
    }

    public void setBatchSn(String batchSn) {
        this.batchSn = batchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getReportNum() {
        return reportNum;
    }

    public void setReportNum(Integer reportNum) {
        this.reportNum = reportNum;
    }

    public String getReportRemark() {
        return reportRemark;
    }

    public void setReportRemark(String reportRemark) {
        this.reportRemark = reportRemark;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockDamageReportProduct that = (StockDamageReportProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(reportId, that.reportId) &&
                Objects.equals(stockSn, that.stockSn) &&
                Objects.equals(batchId, that.batchId) &&
                Objects.equals(batchSn, that.batchSn) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSn, that.productSn) &&
                Objects.equals(specification, that.specification) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(price, that.price) &&
                Objects.equals(costPrice, that.costPrice) &&
                Objects.equals(taxRate, that.taxRate) &&
                Objects.equals(stockNum, that.stockNum) &&
                Objects.equals(reportNum, that.reportNum) &&
                Objects.equals(reportRemark, that.reportRemark) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reportId, stockSn, batchId, batchSn, goodsId, productId, productName, productSn, specification, categoryId, categoryName, unit, price, costPrice, taxRate, stockNum, reportNum, reportRemark, type);
    }

    @Override
    public String toString() {
        return "StockDamageReportProduct{" +
                "id=" + id +
                ", reportId=" + reportId +
                ", stockSn='" + stockSn + '\'' +
                ", batchId=" + batchId +
                ", batchSn='" + batchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", costPrice=" + costPrice +
                ", taxRate=" + taxRate +
                ", stockNum=" + stockNum +
                ", reportNum=" + reportNum +
                ", reportRemark='" + reportRemark + '\'' +
                ", type=" + type +
                '}';
    }
}
