package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 财务收入类型枚举
 *
 * @author 张崧
 * @since 2024-03-18
 */
@Getter
@AllArgsConstructor
public enum FinanceIncomeTypeEnum {

    /**
     * 订单销售收入
     */
    OrderSale,
    /**
     * 供应商退货收入
     */
    SupplierReturn,
    /**
     * 其他收入
     */
    Other,

}
