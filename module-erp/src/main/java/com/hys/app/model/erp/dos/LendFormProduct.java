package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 借出单产品实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_lend_form_product")
@ApiModel
public class LendFormProduct implements Serializable {

    private static final long serialVersionUID = -5877472924451593015L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 借出单ID
     */
    @ApiModelProperty(name = "lend_id", value = "借出单ID")
    private Long lendId;
    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号")
    private String stockSn;
    /**
     * 入库批次ID
     */
    @ApiModelProperty(name = "stock_batch_id", value = "入库批次ID")
    private Long stockBatchId;
    /**
     * 入库批次编号
     */
    @ApiModelProperty(name = "stock_batch_sn", value = "入库批次编号")
    private String stockBatchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 库存数量
     */
    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;
    /**
     * 借出数量
     */
    @ApiModelProperty(name = "lend_num", value = "借出数量")
    private Integer lendNum;
    /**
     * 归还数量
     */
    @ApiModelProperty(name = "return_num", value = "归还数量")
    private Integer returnNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLendId() {
        return lendId;
    }

    public void setLendId(Long lendId) {
        this.lendId = lendId;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getStockBatchId() {
        return stockBatchId;
    }

    public void setStockBatchId(Long stockBatchId) {
        this.stockBatchId = stockBatchId;
    }

    public String getStockBatchSn() {
        return stockBatchSn;
    }

    public void setStockBatchSn(String stockBatchSn) {
        this.stockBatchSn = stockBatchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getLendNum() {
        return lendNum;
    }

    public void setLendNum(Integer lendNum) {
        this.lendNum = lendNum;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LendFormProduct product = (LendFormProduct) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(lendId, product.lendId) &&
                Objects.equals(stockSn, product.stockSn) &&
                Objects.equals(stockBatchId, product.stockBatchId) &&
                Objects.equals(stockBatchSn, product.stockBatchSn) &&
                Objects.equals(goodsId, product.goodsId) &&
                Objects.equals(productId, product.productId) &&
                Objects.equals(productName, product.productName) &&
                Objects.equals(productSn, product.productSn) &&
                Objects.equals(specification, product.specification) &&
                Objects.equals(categoryName, product.categoryName) &&
                Objects.equals(unit, product.unit) &&
                Objects.equals(stockNum, product.stockNum) &&
                Objects.equals(lendNum, product.lendNum) &&
                Objects.equals(returnNum, product.returnNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lendId, stockSn, stockBatchId, stockBatchSn, goodsId, productId, productName, productSn, specification, categoryName, unit, stockNum, lendNum, returnNum);
    }

    @Override
    public String toString() {
        return "LendFormProduct{" +
                "id=" + id +
                ", lendId=" + lendId +
                ", stockSn='" + stockSn + '\'' +
                ", stockBatchId=" + stockBatchId +
                ", stockBatchSn='" + stockBatchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", stockNum=" + stockNum +
                ", lendNum=" + lendNum +
                ", returnNum=" + returnNum +
                '}';
    }
}
