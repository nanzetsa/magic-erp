package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 采购计划实体DTO
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementPlanDTO implements Serializable {

    private static final long serialVersionUID = -9031393000695135158L;

    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID", required = true)
    @NotNull(message = "部门ID不能为空")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称", required = true)
    @NotEmpty(message = "部门名称不能为空")
    private String deptName;
    /**
     * 编制人员ID
     */
    @ApiModelProperty(name = "formation_person_id", value = "编制人员ID", required = true)
    @NotNull(message = "编制人员ID不能为空")
    private Long formationPersonId;
    /**
     * 编制人员
     */
    @ApiModelProperty(name = "formation_person", value = "编制人员", required = true)
    @NotEmpty(message = "编制人员不能为空")
    private String formationPerson;
    /**
     * 编制时间
     */
    @ApiModelProperty(name = "formation_time", value = "编制时间", required = true)
    @NotNull(message = "编制时间不能为空")
    private Long formationTime;
    /**
     * 采购说明
     */
    @ApiModelProperty(name = "procurement_desc", value = "采购说明")
    private String procurementDesc;
    /**
     * 采购计划产品
     */
    @ApiModelProperty(name = "product_list", value = "采购计划产品", required = true)
    @NotNull(message = "采购计划产品不能为空")
    private List<ProcurementPlanProductDTO> productList;
    /**
     * 供应商ID
     */
    @ApiModelProperty(name = "supplier_id", value = "供应商ID")
    @NotNull(message = "供应商ID不能为空")
    private Long supplierId;
    /**
     * 供应商名称
     */
    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    @NotEmpty(message = "供应商名称不能为空")
    private String supplierName;
    /**
     * 计划供应时间
     */
    @ApiModelProperty(name = "supplier_time", value = "计划供应时间")
    @NotNull(message = "计划供应时间不能为空")
    private Long supplierTime;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getFormationPersonId() {
        return formationPersonId;
    }

    public void setFormationPersonId(Long formationPersonId) {
        this.formationPersonId = formationPersonId;
    }

    public String getFormationPerson() {
        return formationPerson;
    }

    public void setFormationPerson(String formationPerson) {
        this.formationPerson = formationPerson;
    }

    public Long getFormationTime() {
        return formationTime;
    }

    public void setFormationTime(Long formationTime) {
        this.formationTime = formationTime;
    }

    public String getProcurementDesc() {
        return procurementDesc;
    }

    public void setProcurementDesc(String procurementDesc) {
        this.procurementDesc = procurementDesc;
    }

    public List<ProcurementPlanProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProcurementPlanProductDTO> productList) {
        this.productList = productList;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getSupplierTime() {
        return supplierTime;
    }

    public void setSupplierTime(Long supplierTime) {
        this.supplierTime = supplierTime;
    }

    @Override
    public String toString() {
        return "ProcurementPlanDTO{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", formationPersonId=" + formationPersonId +
                ", formationPerson='" + formationPerson + '\'' +
                ", formationTime=" + formationTime +
                ", procurementDesc='" + procurementDesc + '\'' +
                ", productList=" + productList +
                ", supplierId=" + supplierId +
                ", supplierName='" + supplierName + '\'' +
                ", supplierTime=" + supplierTime +
                '}';
    }
}
