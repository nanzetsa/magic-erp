package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.GoodsLabelDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品标签详情
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "商品标签详情")
public class GoodsLabelVO extends GoodsLabelDO {

}

