package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 顺序号重置方式
 *
 * @author 张崧
 * @since 2023-11-30
 */
@Getter
@AllArgsConstructor
public enum NoSeqResetTypeEnum {

    /**
     * 按顺序生成（不会重置）
     */
    Normal,
    /**
     * 每年重置顺序号
     */
    Year,
    /**
     * 每月重置顺序号
     */
    Month,
    /**
     * 每天重置顺序号
     */
    Day


}
