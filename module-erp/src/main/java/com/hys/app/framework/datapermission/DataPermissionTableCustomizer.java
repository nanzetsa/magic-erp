package com.hys.app.framework.datapermission;

import com.hys.app.framework.datapermission.dept.DeptDataPermissionRuleCustomizer;
import com.hys.app.model.erp.dos.*;
import com.hys.app.model.system.dos.AdminUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 定制哪些表需要走数据权限
 *
 * @author 张崧
 */
@Configuration
public class DataPermissionTableCustomizer {

    @Bean
    public DeptDataPermissionRuleCustomizer deptDataPermissionRuleCustomizer() {
        return rule -> {
            // 用户管理
            rule.addDeptColumn(AdminUser.class);
            // 仓库管理
            rule.addDeptColumn(WarehouseDO.class);
            // 采购计划管理
            rule.addDeptColumn(ProcurementPlan.class);
            // 入库单管理
            rule.addDeptColumn(WarehouseEntryDO.class);
            // 出库单管理
            rule.addDeptColumn(WarehouseOutDO.class);
            // 供应商退货
            rule.addDeptColumn(SupplierReturnDO.class);
            // 订单退货
            rule.addDeptColumn(OrderReturnDO.class);
            // 订单
            rule.addDeptColumn(OrderDO.class);
            // 借出单
            rule.addDeptColumn(LendForm.class);
            // 换货单
            rule.addDeptColumn(ChangeForm.class);
            // 库存报损单
            rule.addDeptColumn(StockDamageReport.class);
            // 库存盘点单
            rule.addDeptColumn(StockInventory.class);
            // 销售经理
            rule.addDeptColumn(MarketingManagerDO.class);
        };
    }

}
