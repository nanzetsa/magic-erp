package com.hys.app.controller.erp.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.SupplierReturnItemQueryParams;
import com.hys.app.model.erp.vo.SupplierReturnItemVO;
import com.hys.app.service.erp.SupplierReturnItemManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 供应商退货项API
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
@RestController
@RequestMapping("/admin/erp/supplierReturnItem")
@Api(tags = "供应商退货项API")
@Validated
public class SupplierReturnItemManagerController {

    @Autowired
    private SupplierReturnItemManager supplierReturnItemManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<SupplierReturnItemVO> list(SupplierReturnItemQueryParams queryParams) {
        return supplierReturnItemManager.list(queryParams);
    }

}

