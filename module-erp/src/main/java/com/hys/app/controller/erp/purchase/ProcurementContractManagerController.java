package com.hys.app.controller.erp.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.ProcurementContract;
import com.hys.app.model.erp.dto.ProcurementContractDTO;
import com.hys.app.model.erp.dto.ProcurementContractQueryParam;
import com.hys.app.model.erp.vo.ProcurementContractVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.ProcurementContractManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * 采购合同相关API
 * @author dmy
 * 2023-12-05
 */
@Api(description = "采购合同相关API")
@RestController
@RequestMapping("/admin/procurement/contract")
@Validated
public class ProcurementContractManagerController {

    @Autowired
    private ProcurementContractManager procurementContractManager;

    @ApiOperation(value = "查询采购合同分页列表数据")
    @GetMapping
    public WebPage list(ProcurementContractQueryParam params) {
        return procurementContractManager.list(params);
    }

    @ApiOperation(value = "新增采购合同")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增一个合同信息")
    public void add(@RequestBody @Valid ProcurementContractDTO procurementContractDTO) {
        this.procurementContractManager.add(procurementContractDTO);
    }

    @ApiOperation(value = "修改采购合同")
    @PostMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的合同信息")
    public void edit(@PathVariable Long id, @RequestBody @Valid ProcurementContractDTO procurementContractDTO) {
        this.procurementContractManager.edit(id, procurementContractDTO);
    }

    @ApiOperation(value = "批量删除合同")
    @DeleteMapping("/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要删除的合同ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的合同信息")
    public void delete(@PathVariable List<Long> ids) {
        this.procurementContractManager.delete(ids);
    }

    @ApiOperation(value = "查询合同详情信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "合同ID", required = true, dataType = "int", paramType = "path")
    })
    @GetMapping("/{id}")
    public ProcurementContractVO getDetail(@PathVariable Long id) {
        return procurementContractManager.getDetail(id);
    }

    @ApiOperation(value = "获取所有合同信息集合")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status", value = "合同状态 NEW：未执行，EXECUTING：执行中，CLOSED：已关闭", dataType = "String", paramType = "query", allowableValues = "NEW,EXECUTING,CLOSED")
    })
    @GetMapping("/list-all")
    public List<ProcurementContract> listAll(@ApiIgnore String status) {
        return procurementContractManager.listAll(status);
    }

    @ApiOperation(value = "批量执行合同")
    @PostMapping("/execute/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要执行的合同ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "执行ID为[${ids}]的合同信息")
    public void execute(@PathVariable List<Long> ids) {
        this.procurementContractManager.execute(ids);
    }

    @ApiOperation(value = "批量关闭合同")
    @PostMapping("/close/{ids}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "要关闭的合同ID集合", required = true, dataType = "int", paramType = "path", allowMultiple = true)
    })
    @Log(client = LogClient.admin, detail = "关闭ID为[${ids}]的合同信息")
    public void close(@PathVariable List<Long> ids) {
        this.procurementContractManager.close(ids);
    }
}
