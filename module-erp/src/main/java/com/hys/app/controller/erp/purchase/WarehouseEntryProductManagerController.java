package com.hys.app.controller.erp.purchase;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.WarehouseEntryProductQueryParams;
import com.hys.app.model.erp.vo.WarehouseEntryProductVO;
import com.hys.app.service.erp.WarehouseEntryProductManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 入库单商品明细相关API
 *
 * @author 张崧
 * @since 2023-12-05 15:30:10
 */
@RestController
@RequestMapping("/admin/erp/warehouseEntryProduct")
@Api(tags = "入库单商品明细相关API")
@Validated
public class WarehouseEntryProductManagerController {

    @Autowired
    private WarehouseEntryProductManager warehouseEntryProductManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseEntryProductVO> list(WarehouseEntryProductQueryParams queryParams) {
        return warehouseEntryProductManager.list(queryParams);
    }
}

