package com.hys.app.controller.erp.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.ProductQueryParams;
import com.hys.app.model.erp.vo.ProductVO;
import com.hys.app.service.erp.ProductManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 产品相关API
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@RestController
@RequestMapping("/admin/erp/product")
@Api(tags = "产品相关API")
@Validated
public class ProductManagerController {

    @Autowired
    private ProductManager productManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<ProductVO> list(ProductQueryParams queryParams) {
        return productManager.list(queryParams);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public ProductVO getDetail(@PathVariable Long id) {
        return productManager.getDetail(id);
    }
}

