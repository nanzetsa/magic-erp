package com.hys.app.controller.erp.warehouse;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.*;
import com.hys.app.model.erp.enums.WarehouseOutStatusEnum;
import com.hys.app.model.erp.vo.WarehouseOutVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.WarehouseOutManager;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 出库单相关API
 *
 * @author 张崧
 * @since 2023-12-07 16:50:20
 */
@RestController
@RequestMapping("/admin/erp/warehouseOut")
@Api(tags = "出库单相关API")
@Validated
public class WarehouseOutManagerController {

    @Autowired
    private WarehouseOutManager warehouseOutManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<WarehouseOutVO> list(WarehouseOutQueryParams queryParams) {
        return warehouseOutManager.list(queryParams);
    }

    @ApiOperation(value = "出库前预览")
    @PostMapping("/preview")
    public WarehouseOutVO preview(@RequestBody @Valid WarehouseOutPreviewDTO warehouseOutPreviewDTO) {
        return warehouseOutManager.preview(warehouseOutPreviewDTO);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "创建出库单")
    public void add(@RequestBody @Valid WarehouseOutDTO warehouseOutDTO) {
        warehouseOutManager.add(warehouseOutDTO);
    }

//    @ApiOperation(value = "编辑")
//    @PutMapping("/{id}")
//    @Log(client = LogClient.admin, detail = "编辑id为[${id}]出库单")
//    public void edit(@PathVariable Long id, @RequestBody @Valid WarehouseOutDTO warehouseOutDTO) {
//        warehouseOutDTO.setId(id);
//        warehouseOutManager.edit(warehouseOutDTO);
//    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public WarehouseOutVO getDetail(@PathVariable Long id) {
        return warehouseOutManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除ID为[${ids}]的出库单")
    public void delete(@PathVariable List<Long> ids) {
        warehouseOutManager.delete(ids);
    }

    @ApiOperation(value = "审核")
    @PostMapping("/audit/{ids}")
    @Log(client = LogClient.admin, detail = "出库单[${ids}]审核")
    public void audit(@PathVariable List<Long> ids, @NotNull(message = "审核类型不能为空") WarehouseOutStatusEnum status, String remark) {
        warehouseOutManager.audit(ids, status, remark);
    }

    @ApiOperation(value = "发货")
    @PostMapping("/ship")
    @Log(client = LogClient.admin, detail = "出库单[${shipDTO.id}]发货")
    public void ship(@RequestBody @Valid WarehouseOutShipDTO shipDTO) {
        warehouseOutManager.ship(shipDTO);
    }

    @ApiOperation(value = "查询出库单统计分页列表数据")
    @GetMapping("/statistics")
    public WebPage statistics(WarehouseOutStatisticsParam params) {
        return warehouseOutManager.statistics(params);
    }

    @ApiOperation(value = "导出出库单统计列表")
    @GetMapping("/export")
    public void export(HttpServletResponse response, WarehouseOutStatisticsParam params) {
        this.warehouseOutManager.export(response, params);
    }
}

