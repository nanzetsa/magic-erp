package com.hys.app.controller.base;

import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.shoptnt.ApiParams;
import com.hys.app.model.shoptnt.ShopTntApiEnum;
import com.hys.app.service.shoptnt.ShopTntHttpClint;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 地区api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/28 下午7:49
 * @since v7.0
 */
@RestController
@RequestMapping("/test")
@Api(description = "地区API")
public class TestBaseController {

    @Autowired
    private ShopTntHttpClint shopTntHttpClint;

    @GetMapping
    @ApiOperation(value = "test")
    public String getChildrenById(String msg) {
        ApiParams apiParams = new ApiParams();
        apiParams.put("msg_id", "001");
        apiParams.put("type", "Test");
        apiParams.put("content", msg);
        apiParams.put("produce_time", DateUtil.getDateline());
        return shopTntHttpClint.execute(ShopTntApiEnum.MessageReceive, apiParams).getData();
    }

}
