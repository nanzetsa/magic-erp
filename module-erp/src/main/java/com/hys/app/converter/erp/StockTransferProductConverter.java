package com.hys.app.converter.erp;

import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.StockTransferProductDO;
import com.hys.app.model.erp.vo.StockTransferProductVO;
import com.hys.app.model.erp.dto.StockTransferProductDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 调拨单商品明细 Convert
 *
 * @author 张崧
 * @since 2023-12-12 16:34:15
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StockTransferProductConverter {

    StockTransferProductDO convert(StockTransferProductDTO stockTransferProductDTO);
    
    StockTransferProductVO convert(StockTransferProductDO stockTransferProductDO);
    
    List<StockTransferProductVO> convert(List<StockTransferProductDO> list);
    
    WebPage<StockTransferProductVO> convert(WebPage<StockTransferProductDO> webPage);

    default List<StockTransferProductDO> combination(List<StockTransferProductDTO> productList, Map<Long, WarehouseEntryBatchDO> batchMap, Long stockTransferId){
        return productList.stream().map(stockTransferProductDTO -> {
            WarehouseEntryBatchDO batchDO = batchMap.get(stockTransferProductDTO.getWarehouseEntryBatchId());
            StockTransferProductDO stockTransferProductDO = new StockTransferProductDO();

            stockTransferProductDO.setStockTransferId(stockTransferId);
            stockTransferProductDO.setWarehouseEntryId(batchDO.getWarehouseEntryId());
            stockTransferProductDO.setWarehouseEntrySn(batchDO.getWarehouseEntrySn());
            stockTransferProductDO.setWarehouseEntryItemId(batchDO.getWarehouseEntryItemId());
            stockTransferProductDO.setWarehouseEntryBatchId(batchDO.getId());
            stockTransferProductDO.setWarehouseEntryBatchSn(batchDO.getSn());
            stockTransferProductDO.setGoodsId(batchDO.getGoodsId());
            stockTransferProductDO.setProductId(batchDO.getProductId());
            stockTransferProductDO.setProductSn(batchDO.getProductSn());
            stockTransferProductDO.setProductName(batchDO.getProductName());
            stockTransferProductDO.setProductSpecification(batchDO.getProductSpecification());
            stockTransferProductDO.setProductUnit(batchDO.getProductUnit());
            stockTransferProductDO.setProductBarcode(batchDO.getProductBarcode());
            stockTransferProductDO.setCategoryId(batchDO.getCategoryId());
            stockTransferProductDO.setCategoryName(batchDO.getCategoryName());
            stockTransferProductDO.setProductPrice(batchDO.getEntryPrice());
            stockTransferProductDO.setProductCostPrice(batchDO.getProductCostPrice());
            stockTransferProductDO.setTaxRate(batchDO.getTaxRate());
            stockTransferProductDO.setNum(stockTransferProductDTO.getNum());

            return stockTransferProductDO;
        }).collect(Collectors.toList());
    }
}

