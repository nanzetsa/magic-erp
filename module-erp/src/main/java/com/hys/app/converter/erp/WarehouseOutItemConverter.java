package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import com.hys.app.model.erp.dto.WarehouseOutDTO;
import com.hys.app.model.erp.vo.WarehouseOutItemVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * 出库单商品明细 Convert
 *
 * @author 张崧
 * @since 2023-12-07 17:06:32
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface WarehouseOutItemConverter {

    WarehouseOutItemVO convert(WarehouseOutItemDO warehouseOutItemDO);

    List<WarehouseOutItemVO> convert(List<WarehouseOutItemDO> list);

    WebPage<WarehouseOutItemVO> convert(WebPage<WarehouseOutItemDO> webPage);

    default List<WarehouseOutItemDO> combination(WarehouseOutDTO warehouseOutDTO) {
        List<WarehouseOutItemDO> warehouseOutItemList = new ArrayList<>();

        for (WarehouseOutDTO.Order order : warehouseOutDTO.getOrderList()) {
            for (WarehouseOutDTO.OrderItem orderItem : order.getOrderItemList()) {
                for (WarehouseOutDTO.StockBatch stockBatch : orderItem.getBatchList()) {
                    OrderDO orderDO = warehouseOutDTO.getOrderMap().get(order.getOrderId());
                    OrderItemDO orderItemDO = warehouseOutDTO.getOrderItemMap().get(orderItem.getOrderItemId());
                    WarehouseEntryBatchDO batchDO = warehouseOutDTO.getBatchMap().get(stockBatch.getBatchId());

                    WarehouseOutItemDO warehouseOutItemDO = new WarehouseOutItemDO();
                    warehouseOutItemDO.setWarehouseOutId(warehouseOutDTO.getId());

                    warehouseOutItemDO.setWarehouseEntryId(batchDO.getWarehouseEntryId());
                    warehouseOutItemDO.setWarehouseEntrySn(batchDO.getWarehouseEntrySn());
                    warehouseOutItemDO.setWarehouseEntryItemId(batchDO.getWarehouseEntryItemId());
                    warehouseOutItemDO.setWarehouseEntryBatchId(batchDO.getId());
                    warehouseOutItemDO.setWarehouseEntryBatchSn(batchDO.getSn());

                    warehouseOutItemDO.setOrderId(orderDO.getId());
                    warehouseOutItemDO.setOrderSn(orderDO.getSn());

                    warehouseOutItemDO.setOrderItemId(orderItemDO.getId());
                    warehouseOutItemDO.setGoodsId(orderItemDO.getGoodsId());
                    warehouseOutItemDO.setProductId(orderItemDO.getProductId());
                    warehouseOutItemDO.setProductSn(orderItemDO.getProductSn());
                    warehouseOutItemDO.setProductName(orderItemDO.getProductName());
                    warehouseOutItemDO.setProductSpecification(orderItemDO.getProductSpecification());
                    warehouseOutItemDO.setProductUnit(orderItemDO.getProductUnit());
                    warehouseOutItemDO.setCategoryId(orderItemDO.getCategoryId());
                    warehouseOutItemDO.setCategoryName(orderItemDO.getCategoryName());

                    warehouseOutItemDO.setProductPrice(batchDO.getEntryPrice());
                    warehouseOutItemDO.setProductCostPrice(batchDO.getProductCostPrice());
                    warehouseOutItemDO.setTaxRate(batchDO.getTaxRate());
                    warehouseOutItemDO.setOutNum(stockBatch.getOutNum());
                    warehouseOutItemDO.setReturnNum(0);

                    warehouseOutItemList.add(warehouseOutItemDO);
                }
            }
        }

        return warehouseOutItemList;
    }

}

