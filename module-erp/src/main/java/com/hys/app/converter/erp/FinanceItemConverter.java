package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.FinanceItemDO;
import com.hys.app.model.erp.vo.FinanceItemVO;
import com.hys.app.model.erp.dto.FinanceItemDTO;
import com.hys.app.model.erp.vo.FinanceItemExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 财务明细 Converter
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface FinanceItemConverter {
    
    FinanceItemDO convert(FinanceItemDTO financeItemDTO);
    
    FinanceItemVO convert(FinanceItemDO financeItemDO);
    
    List<FinanceItemVO> convertList(List<FinanceItemDO> list);
    
    List<FinanceItemExcelVO> convertExcel(List<FinanceItemVO> list);
    
    WebPage<FinanceItemVO> convertPage(WebPage<FinanceItemDO> webPage);

}

