package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.EnterpriseDO;
import com.hys.app.model.erp.vo.EnterpriseVO;
import com.hys.app.model.erp.dto.EnterpriseDTO;
import com.hys.app.model.erp.vo.EnterpriseExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 企业 Converter
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface EnterpriseConverter {
    
    EnterpriseDO convert(EnterpriseDTO enterpriseDTO);
    
    EnterpriseVO convert(EnterpriseDO enterpriseDO);
    
    List<EnterpriseVO> convertList(List<EnterpriseDO> list);
    
    List<EnterpriseExcelVO> convertExcel(List<EnterpriseVO> list);
    
    WebPage<EnterpriseVO> convertPage(WebPage<EnterpriseDO> webPage);
    
}

