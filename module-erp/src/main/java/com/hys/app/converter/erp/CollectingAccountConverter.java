package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.CollectingAccountDO;
import com.hys.app.model.erp.dto.CollectingAccountDTO;
import com.hys.app.model.erp.vo.CollectingAccountExcelVO;
import com.hys.app.model.erp.vo.CollectingAccountVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

/**
 * 收款账户 Converter
 *
 * @author 张崧
 * 2024-01-24 14:43:18
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CollectingAccountConverter {

    CollectingAccountDO convert(CollectingAccountDTO collectingAccountDTO);

    CollectingAccountVO convert(CollectingAccountDO collectingAccountDO);

    List<CollectingAccountVO> convertList(List<CollectingAccountDO> list);

    List<CollectingAccountExcelVO> convertExcel(List<CollectingAccountVO> list);

    WebPage<CollectingAccountVO> convertPage(WebPage<CollectingAccountDO> webPage);

}

