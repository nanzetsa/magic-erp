package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.PostDO;
import com.hys.app.model.erp.vo.PostVO;
import com.hys.app.model.erp.dto.PostDTO;
import com.hys.app.framework.database.WebPage;

import java.util.List;

/**
 * 岗位 Convert
 *
 * @author 张崧
 * 2023-11-29 17:15:22
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PostConverter {
    PostDO convert(PostDTO postDTO);
    
    PostVO convert(PostDO postDO);
    
    WebPage<PostVO> convert(WebPage<PostDO> webPage);

    List<PostVO> convert(List<PostDO> list);
}

