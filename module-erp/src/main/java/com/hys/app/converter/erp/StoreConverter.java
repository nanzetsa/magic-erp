package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.StoreDO;
import com.hys.app.model.erp.vo.StoreVO;
import com.hys.app.model.erp.dto.StoreDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 门店 Convert
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StoreConverter {

    StoreDO convert(StoreDTO storeDTO);
    
    StoreVO convert(StoreDO storeDO);
    
    List<StoreVO> convert(List<StoreDO> list);
    
    WebPage<StoreVO> convert(WebPage<StoreDO> webPage);
    
}

