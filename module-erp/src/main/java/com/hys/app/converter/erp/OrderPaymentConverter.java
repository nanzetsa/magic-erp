package com.hys.app.converter.erp;

import com.hys.app.model.erp.dos.CollectingAccountDO;
import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.OrderPaymentDO;
import com.hys.app.model.erp.vo.OrderPaymentVO;
import com.hys.app.model.erp.dto.OrderPaymentDTO;
import com.hys.app.model.erp.vo.OrderPaymentExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单支付明细 Converter
 *
 * @author 张崧
 * 2024-01-24 17:00:32
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderPaymentConverter {
    
    OrderPaymentDO convert(OrderPaymentDTO orderPaymentDTO);
    
    OrderPaymentVO convert(OrderPaymentDO orderPaymentDO);
    
    List<OrderPaymentVO> convertList(List<OrderPaymentDO> list);
    
    List<OrderPaymentExcelVO> convertExcel(List<OrderPaymentVO> list);
    
    WebPage<OrderPaymentVO> convertPage(WebPage<OrderPaymentDO> webPage);

    default List<OrderPaymentDO> convert(Long orderId, List<OrderPaymentDTO> paymentList, Map<Long, CollectingAccountDO> collectingAccountMap){
        return paymentList.stream().map(orderPaymentDTO -> {
            CollectingAccountDO collectingAccountDO = collectingAccountMap.get(orderPaymentDTO.getCollectingAccountId());
            OrderPaymentDO orderPaymentDO = new OrderPaymentDO();

            orderPaymentDO.setOrderId(orderId);
            orderPaymentDO.setCollectingAccountId(orderPaymentDTO.getCollectingAccountId());
            orderPaymentDO.setCollectingAccountName(collectingAccountDO.getName());
            orderPaymentDO.setCollectingAccountSn(collectingAccountDO.getSn());
            orderPaymentDO.setPrice(orderPaymentDTO.getPrice());
            orderPaymentDO.setRemark(orderPaymentDTO.getRemark());

            return orderPaymentDO;
        }).collect(Collectors.toList());
    }
}

