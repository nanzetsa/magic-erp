package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.WarehouseEntryAuditPassMessage;

/**
 * 入库单审核通过事件
 *
 * @author 张崧
 * 2024-01-08
 */
public interface WarehouseEntryAuditPassEvent {

    /**
     * 入库单审核通过
     * @param message
     */
    void onWarehouseEntryAuditPass(WarehouseEntryAuditPassMessage message);

}
