package com.hys.app.mq.receiver;

import com.hys.app.mq.event.WarehouseEntryAuditPassEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.WarehouseEntryAuditPassMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 入库单审核通过
 *
 * @author 张崧
 * @since 2024-01-08
 */
@Component
public class WarehouseEntryAuditPassReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<WarehouseEntryAuditPassEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.WAREHOUSE_ENTRY_AUDIT_PASS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.WAREHOUSE_ENTRY_AUDIT_PASS, type = ExchangeTypes.FANOUT)
    ))
    public void receive(WarehouseEntryAuditPassMessage message) {
        if (events != null) {
            for (WarehouseEntryAuditPassEvent event : events) {
                try {
                    event.onWarehouseEntryAuditPass(message);
                } catch (Exception e) {
                    logger.error("入库单审核通过消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
