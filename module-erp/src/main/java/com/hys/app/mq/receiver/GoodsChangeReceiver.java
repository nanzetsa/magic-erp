package com.hys.app.mq.receiver;

import com.hys.app.mq.event.GoodsChangeEvent;
import com.hys.app.model.base.rabbitmq.AmqpExchange;
import com.hys.app.model.erp.dto.message.GoodsChangeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商品变更消息
 *
 * @author 张崧
 * @since 2024-01-19
 */
@Component
public class GoodsChangeReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<GoodsChangeEvent> events;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.ERP_GOODS_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.ERP_GOODS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void receive(GoodsChangeMessage message) {
        if (events != null) {
            for (GoodsChangeEvent event : events) {
                try {
                    event.onGoodsChange(message);
                } catch (Exception e) {
                    logger.error("商品变更消息出错" + event.getClass().getName(), e);
                }
            }
        }
    }
}
