package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.StockStatisticsMessage;

/**
 * 库存统计事件接口
 *
 * @author dmy
 * 2023-12-22
 */
public interface StockStatisticsEvent {

    /**
     * 库存统计
     *
     * @param message 库存统计消息
     */
    void stockStatistics(StockStatisticsMessage message);

}
