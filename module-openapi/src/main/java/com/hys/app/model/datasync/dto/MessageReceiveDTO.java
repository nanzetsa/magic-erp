package com.hys.app.model.datasync.dto;

import com.hys.app.model.datasync.enums.MessageReceiveTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 接收第三方系统消息DTO
 *
 * @author 张崧
 * @since 2024-04-01
 */
@Data
public class MessageReceiveDTO {

    @ApiModelProperty(name = "msg_id", value = "外部系统的消息id，每次请求必须唯一")
    @NotBlank(message = "外部系统的消息id不能为空")
    private String msgId;

    @ApiModelProperty(name = "type", value = "消息类型")
    @NotNull(message = "消息类型不能为空")
    private MessageReceiveTypeEnum type;

    @ApiModelProperty(name = "content", value = "消息内容")
    @NotBlank(message = "消息内容不能为空")
    private String content;

    @ApiModelProperty(name = "produce_time", value = "外部系统生成消息的时间")
    @NotNull(message = "外部系统生成消息的时间不能为空")
    private Long produceTime;

}

