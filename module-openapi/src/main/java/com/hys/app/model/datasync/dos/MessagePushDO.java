package com.hys.app.model.datasync.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.datasync.enums.MessagePushStatusEnum;
import com.hys.app.model.datasync.enums.TargetSystemEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 消息推送实体类
 *
 * @author 张崧
 * @since 2023-12-18 16:24:10
 */
@TableName("erp_message_push")
@Data
public class MessagePushDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "target_system", value = "推送的目标系统")
    private TargetSystemEnum targetSystem;

    @ApiModelProperty(name = "business_type", value = "消息业务类型")
    private String businessType;

    @ApiModelProperty(name = "summary", value = "消息摘要（前端展示和快速定位数据用）")
    private String summary;

    /**
     * todo 大文本字段单独存一张表
     */
    @ApiModelProperty(name = "content", value = "消息内容")
    private String content;

    @ApiModelProperty(name = "produce_time", value = "消息生产时间")
    private Long produceTime;

    @ApiModelProperty(name = "status", value = "消息状态")
    private MessagePushStatusEnum status;

    @ApiModelProperty(name = "fail_count", value = "推送失败次数")
    private Integer failCount;

    @ApiModelProperty(name = "remark", value = "推送备注")
    private String remark;

}
