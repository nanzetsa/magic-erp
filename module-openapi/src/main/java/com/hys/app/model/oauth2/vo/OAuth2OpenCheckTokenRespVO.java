package com.hys.app.model.oauth2.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 开放接口 - 校验令牌 Response VO
 *
 * @author 张崧
 * @since 2024-02-20
 */
@ApiModel(description = "【开放接口】校验令牌 Response VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2OpenCheckTokenRespVO {

    @ApiModelProperty(value = "用户编号")
    @JsonProperty("user_id")
    private Long userId;

    @ApiModelProperty(value = "用户类型，参见 UserTypeEnum 枚举")
    @JsonProperty("user_type")
    private Integer userType;

    @ApiModelProperty(value = "租户编号")
    @JsonProperty("tenant_id")
    private Long tenantId;

    @ApiModelProperty(value = "客户端编号")
    @JsonProperty("client_id")
    private String clientId;

    @ApiModelProperty(value = "授权范围")
    private List<String> scopes;

    @ApiModelProperty(value = "访问令牌")
    @JsonProperty("access_token")
    private String accessToken;

    @ApiModelProperty(value = "过期时间，时间戳 / 1000，即单位：秒")
    private Long exp;

}
