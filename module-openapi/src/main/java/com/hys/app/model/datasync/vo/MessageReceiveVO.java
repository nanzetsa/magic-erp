package com.hys.app.model.datasync.vo;

import com.hys.app.model.datasync.dos.MessageReceiveDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 消息接收VO
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MessageReceiveVO extends MessageReceiveDO {

}

