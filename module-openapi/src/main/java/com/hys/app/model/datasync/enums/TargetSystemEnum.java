package com.hys.app.model.datasync.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息推送的目标系统
 *
 * @author 张崧
 * @since 2024-01-05
 */
@Getter
@AllArgsConstructor
public enum TargetSystemEnum {

    /**
     * 商城系统
     */
    Shop,

}
