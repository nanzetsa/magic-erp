package com.hys.app.service.datasync.push.common;

import com.hys.app.model.base.ThreadPoolConstant;
import com.hys.app.model.datasync.dos.MessagePushDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 消息异步推送
 *
 * @author 张崧
 * @since 2023-12-18
 */
@Service
public class MessagePushAsync {

    @Autowired
    @Lazy
    private MessagePushManager messagePushManager;

    @Async(ThreadPoolConstant.MESSAGE_PUSH_POOL)
    public void push(MessagePushDO messagePushDO) {
        messagePushManager.pushSync(messagePushDO);
    }
}

